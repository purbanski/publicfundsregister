<?php 
//----------------------------------------------------------------
date_default_timezone_set('Europe/Berlin');

$cfg['Timestamp'] 		= date('[Y:m:d][H:i:s]') ."\n";
$cfg['FrontendUrl'] 	= 'http://trup.blackted.com';
$cfg['ClientIP']		= $_SERVER['REMOTE_ADDR'];

$cfg[0]['DBServer'] 	= 'localhost:3307';
$cfg[0]['DBName'] 		= 'TRUP_Szczecin_UM';
$cfg[0]['DBUsername'] 	= 'root';
$cfg[0]['DBPassword'] 	= 'dupadupa';
$cfg[0]['TrupName']		= 'Szczecin';
$cfg[0]['TrupNameLong']	= 'Urz&#261;d Miasta Szczecin';

$cfg[1]['DBServer'] 	= 'localhost:3307';
$cfg[1]['DBName'] 		= 'CRU_MILANOWEK';
$cfg[1]['DBUsername'] 	= 'root';
$cfg[1]['DBPassword'] 	= 'dupadupa';
$cfg[1]['TrupName']		= 'Milan&#243;w';
$cfg[1]['TrupNameLong']	= 'Urz&#261;d Miasta Milan&#243;w';

$cfg[2]['DBServer'] 	= 'localhost:3307';
$cfg[2]['DBName'] 		= 'TRUP_mkidn';
$cfg[2]['DBUsername'] 	= 'root';
$cfg[2]['DBPassword'] 	= 'dupadupa';
$cfg[2]['TrupName']		= 'Ministerstwo Kultury';
$cfg[2]['TrupNameLong']	= 'Ministerstwo Kultury i Dziedzictwa Narodowego';

$cfg[69]['DBServer'] 	= 'localhost:3307';
$cfg[69]['DBName'] 		= 'TRUP_Test';
$cfg[69]['DBUsername'] 	= 'root';
$cfg[69]['DBPassword'] 	= 'dupadupa';
$cfg[69]['TrupName']		= 'TEST';
$cfg[69]['TrupNameLong']	= 'Trup Testowy';
//----------------------------------------------------------------
Class Config
{
	static protected $config = array();
	private function __construct() {} //make this private so we can't instanciate
	public static function set($key, $val)
	{
		Config::$config[ $key ] = $val;
	}

	public static function get($key)
	{
		if ( !isset(Config::$config[ $key ]))
			return "";
		
		return Config::$config[ $key ];
	}
}
//----------------------------------------------------------------
function SetConfig( $cruId )
{
	global $cfg;
	foreach( $cfg[$cruId] as $key => $value )
	{
// 		echo "".$key."   = ". $value ."<br/>";
 		Config::set($key, $value);	
	}	
}
//----------------------------------------------------------------
function SetConfigFromVar()
{
	$id = $_GET['cruId'];
	if (!is_numeric($id))
		die();
	SetConfig( $id );
}
	

?>
