<?php 
// header("Access-Control-Allow-Origin: *"); 
// header("Access-Control-Allow-Methods: POST, GET, OPTIONS, HEAD");
// // header("Access-Control-Allow-Headers: X-Requested-With");
// header("Access-Control-Allow-Headers: *");
// header("Content-Type: application/json");
//------------------------------------------------------------------------------
require_once '../config.php';
require_once '../toolbox.php';
//------------------------------------------------------------------------------

if ( $_SERVER['REQUEST_METHOD'] != 'PUT' ) 
	die("");

$put = file_get_contents("php://input");

$put = explode('&', $put);

$cruId = explode('=',$put[0]);
$cruId = $cruId[1];

$tagsTmp = explode('=',$put[1]);
$tagsTmp = $tagsTmp[1];
$tags = json_decode( $tagsTmp );

$newTmp = explode('=', $put[2]);
$newTmp = $newTmp[1];
$newTags = json_decode( $newTmp );

SetConfig( $cruId );

$con = mysql_connect(Config::get('DBServer'), Config::get('DBUsername'), Config::get('DBPassword'));
if (!$con)
{
	die('Could not connect: ' . mysql_error());
}
mysql_query("SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'", $con);

//----------------------------------------------------------------------------------------------
function my_insert_tags()
{
	global $newTags, $con;

	if (count($newTags) == 0)
		return;
	
	$sqlNewTags = "
		INSERT INTO ". Config::get('DBName') .".backend_tag ( id, tag, opis )
		VALUES
		";

	foreach ($newTags as $row)
	{
		$newConv = get_object_vars($row);
		$sqlNewTags .= "( NULL, '".$newConv['tag']."', '' ), ";
	}
	
	$sqlNewTags = substr($sqlNewTags, 0, strlen($sqlNewTags) - 2);
	$sqlNewTags = $sqlNewTags . ";";
	
// 	echo $sqlNewTags;
	 mysql_query($sqlNewTags,$con);
}
//----------------------------------------------------------------------------------------------
function get_tag_id( $tag )
{
	global $con;

	$sql = "
		SELECT id, tag FROM ". Config::get('DBName') .".backend_tag 
		WHERE tag = '". $tag ."';";

	$result = mysql_query($sql,$con);
	$row = mysql_fetch_array( $result );
	
	return $row['id'];
}
//----------------------------------------------------------------------------------------------	
function update_umowy_with_tag_sql( $umowaId, $tagId, $value )
{
	if ( $value )
	{
		$sqlInsert = "
			INSERT INTO ". Config::get('DBName') .".backend_umowa_tags ( id , umowa_id, tag_id )
			VALUES ( NULL ,  '". $umowaId . "', '". $tagId ."'	);";

		return $sqlInsert;
	}
	
	$sqlDelete = "
			DELETE FROM ". Config::get('DBName') .".backend_umowa_tags
			WHERE ". Config::get('DBName') .".backend_umowa_tags.umowa_id = " . $umowaId . "
			AND ". Config::get('DBName') .".backend_umowa_tags.tag_id = " . $tagId . ";";
	
	return $sqlDelete;
}
//----------------------------------------------------------------------------------------------
function update_umowy_with_tags()
{
	global $tags, $con;
	
	if (count($tags) == 0)
		return;
	
	foreach ( $tags as $k => $v )
	{
		$umowaId = $k; 
		
		$rec = $v;
		foreach ( $rec as $name => $value )
		{
			$vc = get_object_vars($value);
			$tagName = $vc['tag'];
			$tagId = $vc['id'];
			$tagValue = $vc['value'];
			
// 			echo "------------<br/>";
// 			echo "Tag Id: ".$tagId."<br/>";
// 			echo "Tag Name: ".$tagName."<br/>";
// 			echo "Tag Value: ".$tagValue."<br/>";
			
// 			echo $tagId;
			if ( ! isset($tagId) )
				$tagId = get_tag_id($tagName);	
			
			$sql = update_umowy_with_tag_sql( $umowaId, $tagId, $tagValue );
// 			echo $sql."\n";
			mysql_query($sql);
		}
	}
}
//----------------------------------------------------------------------------------------------

//---
my_insert_tags();
update_umowy_with_tags();

//---

// $callback = $_GET['callback'];

// echo $callback.'('.pu_mysql_to_json($result).');';
mysql_close($con);

//------------------------------------------------------------------------------
?>
