<?php 
//---------------------------------------
function puGetData()
{
	$data = strip_tags($_REQUEST['data']);
	$data = json_decode($data);
	
	$dataConv = array();
	foreach ( $data as $key => $value )
		$dataConv[$key] = $value;
	
	return $dataConv;	
}
//----------------------------------------------------------------
function puGetVar($key)
{
	$data = puGetData();
	return $data[$key];
}
//----------------------------------------------------------------
function puSetConfig()
{
	$data = puGetData();
	SetConfig( $data['cruId']);
}
//----------------------------------------------------------------
function puGetSearchUrl()
{
	$dataConv = puGetData();

	// 	$ret = 'http://localhost:8000/app/index.html#';
	$ret = 'http://trup.blackted.com/#';
	$ret .= "/search_v2";
	$ret .= "/".$dataConv['cruId'] ;
	$ret .= "/".$dataConv['search'] ;
	$ret .= "/".$dataConv['dateFrom'];
	$ret .= "/".$dataConv['dateTo'];

	return $ret;
}
//----------------------------------------------------------------

?>