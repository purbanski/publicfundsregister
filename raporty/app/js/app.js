'use strict';

var myApp = angular.module('myApp', [
  'ngRoute',
  'ngSanitize',
  'myApp.filters',
  'myApp.services',
  'myApp.directives',
  'myApp.controllers',
  'ui.date',
  'tagServiceModule',
//  'testService',
//  'myFactory',
//  'myProv'
  ]).
config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/admin', {template: '<div></div>', controller: 'Admin'});
  $routeProvider.when('/home/:cruId?',	 												{templateUrl: 'partials/search/main.html', controller: 'Search'});
//   $routeProvider.when('/home/:cruId?',	 												{templateUrl: 'partials/search/box/main.html', controller: 'TableMain'});
//  $routeProvider.when('/home/:cruId?',	 												{templateUrl: 'partials/search/table/main.html', controller: 'TableMain'});
  $routeProvider.when('/search/:searchString?', 								{templateUrl: 'partials/search/main.html', controller: 'Search'});
  $routeProvider.when('/search/:searchString?/:searchDateFrom?/:searchDateTo?',	{templateUrl: 'partials/search/main.html', controller: 'Search'});

  $routeProvider.when('/single_view/:cruId?/:umowaId1/:umowaId2/:umowaId3',		{templateUrl: 'partials/search/main.html', controller: 'Search'});

  $routeProvider.when('/search_v2/:cruId?/:searchString?', 						{templateUrl: 'partials/search/main.html', controller: 'Search'});
  $routeProvider.when('/search_v2/:cruId?/:searchString?/:searchDateFrom?/:searchDateTo?',	{templateUrl: 'partials/search/main.html', controller: 'Search'});
  
  $routeProvider.when('/stats/by_month/:cruId/:category',	{templateUrl: 'partials/stats/by-months/by-months-main.html', controller: 'StatsAnon'});
  $routeProvider.when('/stats/group_by_company/:cruId',		{templateUrl: 'partials/stats/group-by-company/group-by-company-main.html', controller: 'StatsGroupByCompany'});

  $routeProvider.when('/firma/:firma', {templateUrl: 'partials/firma-detale.html', controller: 'FirmaDetale'});
  $routeProvider.when('/info', {templateUrl: 'partials/info.html', controller: 'Search'});
  
  $routeProvider.otherwise({redirectTo: '/home/0'});
  
}]);

