angular.module('myApp.controllers').
	controller('StatsGroupByCompany', ['$scope', '$sce', '$http', '$routeParams', function($scope, $sce, $http, $routeParams )
	{
	  $scope.templates	= myTemplates;

	  $scope.cruId = $routeParams.cruId;
	  if (typeof($scope.cruId) === "undefined" )
		  $scope.cruId = 0;

	  $scope.registryName = Config[$scope.cruId][0];
	    
	  $scope.FrontendUrl = FrontendUrl;
	  $scope.BackendUrl = BackendUrl;
	  $scope.PHPBackend = PHPBackend;
	  
	  //------------------------------------------------------------
	  //------------------------------------------------------------
	  $http({method:'JSONP', url: PHPBackend+'/getCompanyStats.php?cruId=' + $scope.cruId + '&callback=JSON_CALLBACK'}).
	  	success(function(data) {
		    $scope.companies = data;
		    $scope.reverse = 0;
		    $scope.resortByCount($scope.reverse);
		    $scope.resortFn = $scope.resortByCount;
		    $scope.sortByLabel = "Ilo\u015b\u0107";
	  	}).
		error(function(data, status, headers, config) {
		});
	  
	  //------------------------------------------------------------
	  //------------------------------------------------------------
	  $scope.getShortString = function(str, limit){
		  var ret = str;
		  if (str.length > limit+3)
		  {
			  ret = str.substring(0, limit) + "...";
		  }
		  return ret;
	  };

	  //------------------------------------------------------------
	  //------------------------------------------------------------
	  function sortCenaAsc(a,b) {
		    a = parseInt(a.cena);
		    b = parseInt(b.cena);
		    return a > b ? 1 : (a === b ? 0 : -1);
		}

	  function sortCenaDesc(a,b) {
		    a = parseInt(a.cena);
		    b = parseInt(b.cena);
		    return a < b ? 1 : (a === b ? 0 : -1);
		}

	  $scope.resortByCena = function($desc){
		  if ( $desc )
			  $scope.companies.sort(sortCenaAsc);
		  else 
			  $scope.companies.sort(sortCenaDesc);
	  };

	  //------------------------------------------------------------
	  //------------------------------------------------------------
	  function sortCountAsc(a,b) {
		    a = parseInt(a.count);
		    b = parseInt(b.count);
		    return a > b ? 1 : (a === b ? 0 : -1);
		}

	  function sortCountDesc(a,b) {
		    a = parseInt(a.count);
		    b = parseInt(b.count);
		    return a < b ? 1 : (a === b ? 0 : -1);
		}

	  $scope.resortByCount = function($desc){
		  if ( $desc )
			  $scope.companies.sort(sortCountAsc);
		  else 
			  $scope.companies.sort(sortCountDesc);
	  };

	  //------------------------------------------------------------
	  //------------------------------------------------------------
	  function sortCompanyAsc(a,b)
	  {
		  if (a.firma < b.firma) return -1;
		  if (a.firma > b.firma) return 1;
		  return 0;
	  }

	  function sortCompanyDesc(a,b)
	  {
		  if (a.firma > b.firma) return -1;
		  if (a.firma < b.firma) return 1;
		  return 0;
	  }

	  $scope.resortByCompany = function($desc){
		  if ( $desc )
			  $scope.companies = $scope.companies.sort(sortCompanyAsc);
		  else 
			  $scope.companies = $scope.companies.sort(sortCompanyDesc);
	  };

	  //------------------------------------------------------------
	  //------------------------------------------------------------

	  $scope.getSortDirectionIcon = function(reverse)
	  {
		  if (!reverse)
			  return "glyphicon-arrow-up";
		  else
			  return "glyphicon-arrow-down";
	  }
  }]);

