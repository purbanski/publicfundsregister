angular.module('myApp.controllers').
	controller('StatsAnon', ['$scope', '$sce', '$http', '$routeParams', function($scope, $sce, $http, $routeParams )
	{

		function setShortCategory()
		{
			if ($scope.category.length > 7 )
			{
				tmp = $scope.category.substring(0,6);
				tmp += "...";
				$scope.shortCategory = tmp;
			}
			else
				$scope.shortCategory = $scope.category;
			
			$scope.shortCategory = puCapitaliseFirstLetter($scope.shortCategory);
		}
		
//	  $scope.templates.common.top_menu	 	= { name: 'comp-top-menu', url: 'partials/common/top-menu.html'};
	  $scope.templates	= myTemplates;
	  $scope.template	= $scope.templates.search.table.main;

	  $scope.cruId = $routeParams.cruId;
	  if (typeof($scope.cruId) === "undefined" )
		  $scope.cruId = 0;

	  $scope.registryName = Config[$scope.cruId][0];

	  $scope.category = $routeParams.category;
	  if (typeof($scope.category) === "undefined" )
		  $scope.category = 0;
	  
	  setShortCategory();
	  
	  //---------
	  $http({method:'JSONP', url: PHPBackend+'/getAllRecordsWithAddons.php?cruId=' + $scope.cruId + '&callback=JSON_CALLBACK'}).
//	  $http({method:'JSONP', url: PHPBackend+'/getAllRecordsForFirma.php?cruId=' + $scope.cruId + '&firma=' + $scope.firma + '&callback=JSON_CALLBACK'}).
	  	success(function(data) {
//		    angular.forEach(data, function(value, key){
//		    	data[key].id = parseInt(data[key].id);
//		    	data[key].cena = parseFloat(data[key].cena);
//		    	var id = data[key].id;
//		    	$scope.showCommentsContainer[id] = false;
//		    });
		    $scope.umowy = data;
		    $scope.getStats();
		    
//		

	  	}).
		error(function(data, status, headers, config) {
		});
	  
	  $scope.emptyMonthTable = function(){
		  var table = [
		           { month: 0, label: 'total', totalPrice : 0, totalCount : 0, categoryPrice: 0, categoryCount: 0, priceProportion: 0, countProportion: 0 },
		           { month: 1, totalPrice : 0, totalCount : 0, categoryPrice : 0, categoryCount : 0 },
		           { month: 2, totalPrice : 0, totalCount : 0, categoryPrice : 0, categoryCount : 0 },
		           { month: 3, totalPrice : 0, totalCount : 0, categoryPrice : 0, categoryCount : 0 },
		           { month: 4, totalPrice : 0, totalCount : 0, categoryPrice : 0, categoryCount : 0 },
		           { month: 5, totalPrice : 0, totalCount : 0, categoryPrice : 0, categoryCount : 0 },
		           { month: 6, totalPrice : 0, totalCount : 0, categoryPrice : 0, categoryCount : 0 },
		           { month: 7, totalPrice : 0, totalCount : 0, categoryPrice : 0, categoryCount : 0 },
		           { month: 8, totalPrice : 0, totalCount : 0, categoryPrice : 0, categoryCount : 0 },
		           { month: 9, totalPrice : 0, totalCount : 0, categoryPrice : 0, categoryCount : 0 },
		           { month: 10, totalPrice : 0, totalCount : 0, categoryPrice : 0, categoryCount : 0},
		           { month: 11, totalPrice : 0, totalCount : 0, categoryPrice : 0, categoryCount : 0 },
		           { month: 12, totalPrice : 0, totalCount : 0, categoryPrice : 0, categoryCount : 0 }];
		  
		  return table;
	  }

	  
//---------------------------------------------------------------------------
	  $scope.getStats = function()
	  {
		  
		  $scope.stats = {};
		  $scope.statsCategory = {};

		  $scope.stats.years = [2013,2014];

		  for ( i =0 ; i < $scope.stats.years.length; i++)
		  {
			  var y = $scope.stats.years[i];
			  
			  $scope.stats[y] = $scope.emptyMonthTable();
			  $scope.statsCategory[y] = $scope.emptyMonthTable(); 
		  }
		 
		  angular.forEach($scope.umowy, function(umowa, key){
			  var data;
			  var cena;
			  var element;
			  var month;
			  var year;
			  
			  data = umowa.data.split("-");
			  year = parseInt(data[0]);
			  month = parseInt(data[1]);

			  cena = parseFloat( umowa.cena );
			  element = $scope.stats[year][month];
			  elementTotal = $scope.stats[year][0];
			  
			  if (typeof(element)==="undefined")
			  {
			  }
			  else
			  {
				  var pattern = $scope.category;
				  var regex = new RegExp(pattern, 'i');
				  
				  element.totalPrice += cena;
				  element.totalCount++;
				  
				  elementTotal.totalPrice += cena;
				  elementTotal.totalCount++;
				  
				  if ( umowa.zlecenioBiorca.match(regex) )
				  {
					  element.categoryPrice += cena;
					  element.categoryCount++;
					  
					  elementTotal.categoryPrice += cena;
					  elementTotal.categoryCount++;

				  }
				  
				  $scope.stats[parseInt(data[0])][parseInt(data[1])] = element;
			  }
				  
		    });
		  
		  for ( i =0 ; i < $scope.stats.years.length; i++)
		  {
			  var y = $scope.stats.years[i];
			  var elem =  $scope.stats[y][0];
			  
			  elem.countProportion = elem.categoryCount / elem.totalCount * 100;
			  elem.priceProportion = elem.categoryPrice / elem.totalPrice * 100;
		  }
		  
	  }

	//---------------------------------------------------------------------------
	  $scope.getYearStats = function(yearId)
	  {
		  if ( typeof($scope.stats) === "undefined" || typeof($scope.stats[yearId]) === "undefined")
			  return;
		  
		  return $scope.stats[yearId];
	  }
	  

	  
	  //---------------------------------------------------------------------------

//	   $scope.test = function( $fileCount ){
//		  alert('test');
//		  if ( $fileCount != null && $fileCount > 0 )
//			  return "";
//		  else
//			  return "btn";
//		  $scope.viewFilesButtonn = '<button>dupa</button>';
//	  };	
  }]);


angular.module('myApp.controllers').
controller('StatsByMonth', ['$scope', function($scope)
{
	$scope.init = function( yearId )
	{
		$scope.yearId = yearId;
	}
}]);
