


var myApp = angular.module('myApp.controllers', []);
//angular.module('myApp', []).factory( 'myFactory', MyFunc );


  myApp.directive('whenScrolled', function() {
	  
	    return function(scope, elm, attr) {
	        var fromBottom = 300;
	        $(window).bind('scroll', function() {
//	        	var tmp
//	        	tmp = $(window).scrollTop() + $(window).height();
//	        	console.log($(window).scrollTop() + "+"+ $(window).height() +"=" + tmp + " == "+$(document).height());
	        	
	        	if( $(window).scrollTop() + $(window).height() >= $(document).height() - fromBottom) 
	        	{
        	       scope.$apply(attr.whenScrolled);    
	        	}
	        });
	    };
	});
  
	myApp.directive('whenTop', function() {
	    return function(scope, elm, attr) {
	        var fromTop = 150;
	        $(window).bind('scroll', function() {
	        	if ( $(window).scrollTop() < fromTop )
	        	{
	        		scope.$apply(attr.whenTop);  
	        	}
	        });
	    };
	});
	
	
	myApp.controller('Search', ['$scope', '$sce', '$http', '$routeParams', 'tagServiceProvider', 
	                            function($scope, $sce, $http, $routeParams, Tags  ){

		$scope.templates	= myTemplates;
	  $scope.template	= $scope.templates.search.table.main;
	  
	  $scope.obecnaStrona = 0;
	  $scope.displayEntriesLimit = 50;
	  $scope.infiniteScroll = 1;
	  $scope.cruId = $routeParams.cruId;
	  
	  if (typeof($scope.cruId) === "undefined" )
		  $scope.cruId = 0;
	  $scope.registryName = Config[$scope.cruId][0];

	  
		$scope.tags = new Tags( $scope.cruId );
	
//		$scope.tags = new Tags( );
//		$scope.factoryOutput = "myFacto2ry = " + $scope.tags.bong();

		
//	  console.log("CRU id:", $scope.cruId);
	  
	  $scope.showCommentsContainer = [];
	  $scope.showCommentsButton = [];
	  $scope.showFilesContainer = [];
	  $scope.filesForUmowa = [];
	  $scope.commentData = [];
	  $scope.komentarze = [];
	  
	  $scope.newTags = [];
	  
	  $scope.sortByLabel = 'Data';
	  $scope.sortBy = '';
	  $scope.resortFn = $scope.resortUmowyByData;
//	  $scope.reverse = 1;
	  $scope.switchSearchButtonLabel = "Tabelka";
	  
	 	  
	  $http({method:'JSONP', url: PHPBackend+'/getAllRecordsWithAddons.php?cruId=' + $scope.cruId + '&callback=JSON_CALLBACK'}).
	  	success(function(data) {
		    angular.forEach(data, function(value, key){
		    	data[key].id = parseInt(data[key].id);
		    	data[key].cena = parseFloat(data[key].cena);
		    	var id = data[key].id;
		    	$scope.showCommentsContainer[id] = false;
		    	$scope.showCommentsButton[data[key].umowaId] = "da";
		    });
		    
	
		    
		    $scope.umowy = data;
		    $scope.umowyInit = jQuery.extend(true, {}, data);
		    $scope.obecnaStrona = 0;
		    $scope.iloscStron = Math.floor($scope.umowy.length / $scope.displayEntriesLimit);
		    $scope.displayFontSize = 90;


		    $scope.FrontendUrl = FrontendUrl;
		    $scope.BackendUrl = BackendUrl;
		    $scope.PHPBackend = PHPBackend;

		    var update = false;

		    if ( $routeParams.searchString )
		    {
		    	$scope.searchString = $routeParams.searchString;
		    	update = true;
		    }

		    if ( $routeParams.searchDateFrom )
		    {
		    	$scope.searchDateFrom = $routeParams.searchDateFrom;
		    	update = true;
		    }
		    
		    if ( $routeParams.searchDateTo )
		    {
		    	$scope.searchDateTo = $routeParams.searchDateTo;
		    	update = true;
		    }
		    
		    if ( $routeParams.umowaId1 && $routeParams.umowaId2 && $routeParams.umowaId3 )
		    {
		    	$scope.searchString = $routeParams.umowaId1 + "/";
		    	$scope.searchString += $routeParams.umowaId2 + "/";
		    	$scope.searchString += $routeParams.umowaId3 ;
		    	
		    	update = true;
		    }
		    
		    
		    if ( update )
		    	$scope.searchByUmowaOpis();
		    	
		    $scope.setSearchSwitchLabel();
		    $scope.updateTotalPrice();

	  	}).
		error(function(data, status, headers, config) {
		});
	  
	  $scope.addNewTag = function( tag )
	  {
		  if (typeof(tag) === "undefined" )
			  return;
		  
		  $scope.newTag = "";
		  console.log(tag);
		  $scope.newTags[ $scope.newTags.length ] =  tag;
	  }
	  //------------------------------------------------------------
	  $scope.resetInfiniteScroll = function() {
		  $scope.infiniteScroll = 1;
	  }
	  //------------------------------------------------------------ 
	  $scope.updatePageCount = function(){
		  $scope.obecnaStrona = 0;
		  if ( $scope.umowy == undefined )
			  return;
		  
		  $scope.iloscStron = Math.floor($scope.umowy.length / $scope.displayEntriesLimit);
	  };	
	  
	  $scope.getSortDirectionIcon = function(reverse)
	  {
		  if (!reverse)
			  return "glyphicon-arrow-up";
		  else
			  return "glyphicon-arrow-down";
	  }
	  //------------------------------------------------------------
	  $scope.setRegistry = function(){
		  $scope.registryName = Config[$scope.cruId][0];
	  };	

	  
	  //------------------------------------------------------------
	  $scope.setLastPage 	= function(){ $scope.obecnaStrona = $scope.iloscStron; 	$scope.sendEvent('PageNavigate','Last'); }	
	  $scope.setFirstPage 	= function(){ $scope.obecnaStrona = 0; 					$scope.sendEvent('PageNavigate','First'); }
	  $scope.setPrevPage 	= function(){ $scope.obecnaStrona--; 					$scope.sendEvent('PageNavigate','Prev'); }
	  $scope.setNextPage 	= function(){ $scope.obecnaStrona++; 					$scope.sendEvent('PageNavigate','Next'); }	

	  $scope.setFontSmaller	= function(){ $scope.displayFontSize -= 10; 	$scope.sendEvent('Zoom','Out'); }
	  $scope.setFontBigger	= function(){ $scope.displayFontSize += 10; 	$scope.sendEvent('Zoom','In'); }
	  
	  $scope.inifiniteScrollInc = function(){ $scope.infiniteScroll++; }
	  
	  $scope.setEntriesLimit = function( $limit ){ $scope.displayEntriesLimit = $limit; $scope.updatePageCount(); $scope.sendEvent('Entries',$limit); }
	  
	  $scope.setDateTo = function( $data ) {
		  $scope.searchDateTo = $data;
		  $scope.searchByUmowaOpis();
		  $scope.sendEvent('Search','DateTo');
		  $scope.resetInfiniteScroll();
	  }

	  $scope.setDateFrom = function( $data ) {
		  $scope.searchDateFrom = $data;
		  $scope.searchByUmowaOpis();
		  $scope.sendEvent('Search','DateFrom');
		  $scope.resetInfiniteScroll();
	  }

	  $scope.setSearchString = function( $data ) {
		  $scope.searchString = $data;
		  $scope.searchByUmowaOpis();
		  $scope.sendEvent('Search','SearchText');
		  $scope.resetInfiniteScroll();
	  }

	  //------------------------------------------------------------
	  $scope.setRegistry = function(){
		  $scope.rejestrNazwa = Config[$scope.cruId][0];
	  };	
	  //------------------------------------------------------------
	  $scope.getShortString = function(str, limit){
		  var ret = str;
		  if (str.length > limit+3)
		  {
			  ret = str.substring(0, limit) + "...";
		  }
		  return ret;
	  };
	  
	  //------------------------------------------------------------
	  $scope.searchReset = function(){
		  $scope.searchDateTo = undefined;
		  $scope.searchDateFrom = undefined;
		  $scope.searchString = undefined;
		  $('#SearchString').val('');
		  $('#SearchDateTo').val('');
		  $('#SearchDateFrom').val('');
		  $scope.searchByUmowaOpis();
		  $scope.resetInfiniteScroll();
	  };	
  
	  //------------------------------------------------------------
	  $scope.switchSearchDisplay = function(){
		if ($scope.template === $scope.templates.search.table.main)
		{
			$scope.template = $scope.templates.search.box.main
		}
		else
		{
			$scope.template = $scope.templates.search.table.main
		}
		$scope.setSearchSwitchLabel();
		$scope.resetInfiniteScroll();
	  }
	
	  //------------------------------------------------------------
	  $scope.setSearchSwitchLabel = function(){
			if ($scope.template === $scope.templates.search.table.main)
			{
				$scope.switchSearchButtonLabel = "Okienka";
			}
			else
			{
				$scope.switchSearchButtonLabel = "Tabelka";
			}
			  
		  }
	  //------------------------------------------------------------
	  $scope.clipboardHelper = function() {
		  var url;
		  var search;
		  var searchDateFrom;
		  var searchDateTo;
			  
		if ( typeof($scope.searchString) === "undefined" )
			search = "";
		else
			search = $scope.searchString;
		  
		if ( typeof($scope.searchDateFrom) === "undefined" )
			searchDateFrom = "";
		else
			searchDateFrom = $scope.searchDateFrom;
		
		if ( typeof($scope.searchDateTo) === "undefined" )
			searchDateTo = "";
		else
			searchDateTo = $scope.searchDateTo;
			
		
			  url = $scope.FrontendUrl + "/#/search_v2/"+$scope.cruId+"/"+search;
			  url += "/" + searchDateFrom;
			  url += "/" + searchDateTo;
		  
//		  "\",\"dateFrom\":\""+ searchDateFrom +
//		  "\", \"dateTo\":\""+  searchDateTo +"\"}";
		  
		  window.prompt("Nacisnij Ctrl+C zeby kopiowac.", url);
	  }
	  
	  
	  
		
	  $scope.processComment = function( comment )
	  {
//		  console.log("raw"+comment);
		  function replacer(match, p1, offset, string)
		  {
			// p1 is nondigits, p2 digits, and p3 non-alphanumerics
			var urlShort;
//			urlShort = p1.replace(/http:)
//			urlShort = p1.replace(/https?:\/\/()/, "$1");
			urlShort = "link";
//			console.log("url:"+p1);
			var str = '<a href="' + p1 + '" target="_blank">';
			str += urlShort+'</a>';
			
			return str;
		  };
			
		  var re = /(https{0,1}:\/\/[^\s]*)/;
		  var newstr = comment.replace(re, replacer);
//		  console.log(newstr);
		  return newstr;
	  }
	  
	  $scope.getNick = function($nick) {
		  if ($nick.length == 0 || $nick == "undefined" )
			  $scope.nick = "(nie podano)";
		  else 
			  $scope.nick = $nick;
	  }
	  	  

	  $scope.getCommentData = function($data) {
		  var tmp = [];
		  tmp = $data.split(" ");
		  tmp[1] = tmp[1].substring(0,tmp[1].length-3);
		  
		  $scope.commentData = tmp[0]+"  "+tmp[1];
	  }
		
	  $scope.submitComment = function($umowaId) {
		  if ( ! this.commentTxt )
		  {
			  this.error = 1;
			  this.showError = 1;
			  return;
		  }

		  var commentTxt =  (this.commentTxt);
		  var nick = (this.commentNick);
		  
		  this.commentTxt = '';
		  this.commentNick = '';
		  		  
		  $http({method:'JSONP', url: PHPBackend+'/postCommentForUmowa.php?cruId='+$scope.cruId+'&uid='+$umowaId+'&nick='+nick+'&comment='+ commentTxt +'&callback=JSON_CALLBACK'}).
		  		success(function(data) {
		  			$scope.komentarze[$umowaId] = data;
		  		}).
				error(function(data, status, headers, config) {
//					 this.commentTxt = $scope.tmp.lastComment;
//					alert("Komentarz nie zostal wyslany");
				});
	  };

	  $scope.$on("updateItems",function(data){
		  $scope.komentarze = data;
	  });
	  
	  //------------------------------------------------------------	  
	  $scope.getShortUmowaStr = function(umowa) {
		  $scope.shortUmowaId = umowa.replace(/CRU\/1[345]\/0+/g,'')
		  $scope.shortUmowaId = $scope.shortUmowaId.replace(/CRU\/MKIDN\//g,'')
		  
	  };
	  
	  //------------------------------------------------------------	  
	  $scope.getCommentsAndFilesForUmowa = function($umowaId) {
		  
		  $scope.showCommentsContainer[$umowaId] = ! $scope.showCommentsContainer[$umowaId];
		  
		  $scope.setTableShowCommentButton($umowaId);
		  
		  if ( ! $scope.showCommentsContainer[$umowaId] )
			  return;

//		  angular.forEach($scope.showCommentsContainer, function(value, key){
//			  $scope.showCommentsContainer[key]=false;
//		    });
		  
		  $http({method:'JSONP', url: PHPBackend+'/getCommentsForUmowa.php?cruId='+$scope.cruId+'&uid='+$umowaId+'&callback=JSON_CALLBACK'}).
		  	success(function(data) {
		  		$scope.komentarze[$umowaId] = data;
				$scope.showCommentsContainer[$umowaId] = true;
		  	}).
			error(function(data, status, headers, config) {
			});
	  
		  $http({method:'JSONP', url: PHPBackend+'/getAllFilesForRecord.php?cruId='+$scope.cruId+'&uid='+$umowaId+'&callback=JSON_CALLBACK'}).
	  		success(function(data) {

	  			$scope.filesForUmowa[$umowaId] = data;
	  			$scope.showFilesContainer = [];
//	  			$scope.showFilesContainer[$umowaId] = true;
//	  			$scope.showCommentsContainer[$umowaId] = false;
		  }).
		error(function(data, status, headers, config) {
		});
	  };

	  
	  //------------------------------------------------------------	  
	  $scope.getCommentsForUmowa = function($umowaId) {
		  $scope.showCommentsContainer[$umowaId] = ! $scope.showCommentsContainer[$umowaId];
		  if ( ! $scope.showCommentsContainer[$umowaId] )
			  return;
		  
		  $http({method:'JSONP', url: PHPBackend+'/getCommentsForUmowa.php?cruId='+$scope.cruId+'&uid='+$umowaId+'&callback=JSON_CALLBACK'}).
		  	success(function(data) {
		  		console.log(data);
		  		$scope.komentarze[$umowaId] = data;
				$scope.showCommentsContainer[$umowaId] = true;
			    $scope.showFilesContainer[$umowaId] = false;
		  	}).
			error(function(data, status, headers, config) {
			});
	  };

	  //------------------------------------------------------------	  
	  $scope.hack = function() 
	  {
		  return "div";
	  }
	  
	  
	  //------------------------------------------------------------
	  $scope.setTableShowCommentButton =  function( umowaId )
	  {
		  if ( $scope.showCommentsContainer[umowaId] )
			  $scope.showCommentsButton[umowaId] = "Schowaj";
		  else
			  $scope.showCommentsButton[umowaId] = 'Poka\u017c';
	  }
	  
	  
	  //------------------------------------------------------------	  
	  $scope.getShowCommentButton = function($umowaId, $count) {
		  if ( $scope.showCommentsContainer[$umowaId] )
			  return "schowaj koment...";
		  
		  var commentText = "test";
		  switch($count)
		  {
		  case "1":
			  commentText = " komentarz";
			  break;
					  
		  case "2":
		  case "3":
		  case "4":
			commentText =" komentarze";
			break;
					
		  default:
			  commentText = " komentarzy";
		  		break;
		  }
		  
		  if ($count==null)
			  $count = 0;
		  
		return $count + commentText;
	}
	  
	  $scope.getFirmaLink = function(firma) {
		  $scope.firmaLink = "#firma/"+firma;
	  }
	  
	  //------------------------------------------------------------
	  $scope.resortUmowyByCena = function($desc){
		  var queryResult;
		  if ($desc)
			 queryResult = Enumerable.From($scope.umowy)
			 .OrderByDescending(function (x) { return x.cena })
			 .ToArray();
		  else
			  queryResult = Enumerable.From($scope.umowy)
				 .OrderBy(function (x) { return x.cena })
				 .ToArray();
			  
	    $scope.umowy = queryResult;
	  };	  

	  //------------------------------------------------------------
	  $scope.resortUmowyByComments = function($desc){
		  var queryResult;
		  if ($desc)
			 queryResult = Enumerable.From($scope.umowy)
			 .OrderByDescending(function (x) { return x.commentsCount })
			 .ToArray();
		  else
			  queryResult = Enumerable.From($scope.umowy)
				 .OrderBy(function (x) { return x.commentsCount })
				 .ToArray();
			  
	    $scope.umowy = queryResult;
	  };	  
	  
	  //------------------------------------------------------------
	  $scope.resortUmowyByData = function($desc){
		  var queryResult;
		  if ($desc)
			 queryResult = Enumerable.From($scope.umowy)
			 .OrderByDescending(function (x) { return x.data })
			 .ToArray();
		  else
			  queryResult = Enumerable.From($scope.umowy)
				 .OrderBy(function (x) { return x.data })
				 .ToArray();
			  
	    $scope.umowy = queryResult;
	  };	  

	  //------------------------------------------------------------
	  $scope.resortUmowyByUmowa = function($desc){
		  var queryResult;
		  if ($desc)
			 queryResult = Enumerable.From($scope.umowy)
			 .OrderByDescending(function (x) { return x.umowa })
			 .ToArray();
		  else
			  queryResult = Enumerable.From($scope.umowy)
				 .OrderBy(function (x) { return x.umowa })
				 .ToArray();
			  
	    $scope.umowy = queryResult;
	  };	  

	  //------------------------------------------------------------
	  $scope.resortUmowyByFirma = function($desc){
		  var queryResult;
		  if ($desc)
			 queryResult = Enumerable.From($scope.umowy)
			 .OrderByDescending(function (x) { return x.zlecenioBiorca })
			 .ToArray();
		  else
			  queryResult = Enumerable.From($scope.umowy)
				 .OrderBy(function (x) { return x.zlecenioBiorca })
				 .ToArray();
			  
	    $scope.umowy = queryResult;
	  };	  

	  //------------------------------------------------------------
	  $scope.resortUmowyByOpis = function($desc){
		  var queryResult;
		  if ($desc)
			 queryResult = Enumerable.From($scope.umowy)
			 .OrderByDescending(function (x) { return x.opis })
			 .ToArray();
		  else
			  queryResult = Enumerable.From($scope.umowy)
				 .OrderBy(function (x) { return x.opis })
				 .ToArray();
			  
	    $scope.umowy = queryResult;
	  };	  

	  //------------------------------------------------------------
	  $scope.resortUmowyByUmowaId = function($desc){
		  var queryResult;
		  if ($desc)
			 queryResult = Enumerable.From($scope.umowy)
			 .OrderByDescending(function (x) { return x.umowaId })
			 .ToArray();
		  else
			  queryResult = Enumerable.From($scope.umowy)
				 .OrderBy(function (x) { return x.umowaId })
				 .ToArray();
			  
	    $scope.umowy = queryResult;
	  };	  

	  //------------------------------------------------------------
	  $scope.resortUmowyByPliki = function($desc){
		  var queryResult;
		  if ($desc)
			 queryResult = Enumerable.From($scope.umowy)
			 .OrderByDescending(function (x) { return x.plikCount })
			 .ToArray();
		  else
			  queryResult = Enumerable.From($scope.umowy)
				 .OrderBy(function (x) { return x.plikCount })
				 .ToArray();
			  
	    $scope.umowy = queryResult;
	  };	  

	  //------------------------------------------------------------
	  $scope.getFilesForRecord = function( $umowaId ) {
		  
		  $scope.showFilesContainer[$umowaId] = ! $scope.showFilesContainer[$umowaId];

		  if ( ! $scope.showFilesContainer[$umowaId] )
			  return;

//		  $scope.showCommentsButton[$umowaId] = "schowaj";
		  
		  $http({method:'JSONP', url: PHPBackend+'/getAllFilesForRecord.php?cruId='+$scope.cruId+'&uid='+$umowaId+'&callback=JSON_CALLBACK'}).
		  	success(function(data) {
		  		$scope.filesForUmowa[$umowaId] = data;
			    $scope.showFilesContainer[$umowaId] = true;
			    $scope.showCommentsContainer[$umowaId] = false;
			    
//			    console.log($scope.showFilesContainer[$umowaId]);
			  }).
			error(function(data, status, headers, config) {
			});
		   
	  };

	  //------------------------------------------------------------	  
	  $scope.getShowFilesButton = function($umowaId, $count) {
		  if ( $scope.showFilesContainer[$umowaId] )
			  return "schowaj pliki";

		  var commentText;
		  switch($count)
		  {
		  case	"1":
			  commentText = " plik";
			  break;
					  
		  case "2":
		  case "3":
		  case "4":
			commentText =" pliki";
			break;
					
		  default:
			commentText = " plik\u00f3w";
		  	break;
		  }
		  
		  if ( $count == null )
			  $count = 0;
		return $count + commentText;
	}

	  //------------------------------------------------------------
	  $scope.getBaseFilename = function( $fullpath )  {
		  var filename = $fullpath.replace(/^.*[\\\/]/, '');
		  return filename;
	  };
	  
	  //------------------------------------------------------------
	  $scope.toggleRow = function( $toggle ) {
		  $scope.toggle = ! $scope.toggle;
		  //http://cru.blackted.com/backend/django/upload/{{getBaseFilename(plik.plik);}}
	  }

	  //------------------------------------------------------------	  
//	  $scope.setUmowyForPage = function( $umowy ) {
//		  console.log("bong setUmowy");
////		  $scope.umowyForPage = $umowy;
//	  }
	  
	  $scope.getUmowyForPage = function( $umowy ) {
	
		  return $umowy;
		  console.log("umowy for page get returinging");
  
		  if ( typeof($umowy) === "undefined" )
		  {
			  console.log("umowy for page undefined returinging");
			  return;
		  }
		  var tripple = new Array();
		  var count = 0;
		  var breakCount = 3;
		  var out = new Array();
		  var index = 0;
//		  
		  for ( var i = 0; i < $umowy.length; i++ )
		  {
			  tripple[count++] = $umowy[i];
			  if ( count == breakCount )
			  {
				  count = 0;
				  out[index++] = tripple;
				  tripple = new Array();
			  }
		  }
		  $scope.umowyForPage = out;

		  return out;
	  }
	  //---------------------------------------------------------
	  $scope.sendPage = function(page)
	  {
//		  ga('send', 'pageview', page);
	  }
	  
	  //---------------------------------------------------------
	  $scope.sendEvent = function(category, event)
	  {
//		  ga('send', 'event', category, event);
	  }

	  //---------------------------------------------------------
	  $scope.searchByUmowaOpis = function(){
		  var tmp = [];
		  var count = 0;

		  $scope.umowy.splice(0, $scope.umowy.length);
		  angular.forEach( $scope.umowyInit, function(value, key){
			  var matchString;
			  var matchDate;
		  
			  matchString = false;
			  matchDate = false;

			  //-----------------
			  // Check data match
			  var dateFrom;
			  var dateTo;
			  var dateUmowa;
			  
			  dateFrom 	= puGetDataSafe($scope.searchDateFrom, '0000-00-00');
			  dateTo 	= puGetDataSafe($scope.searchDateTo, '2100-01-01');
			  dateUmowa = puGetData($scope.umowyInit[key].data);
			  
			  
			  if ( dateFrom.getTime() <= dateUmowa.getTime() & 
					  dateUmowa.getTime() <= dateTo.getTime() )
			  {
				  matchDate = true;
			  }
			  else 
			  {
				  // data don't match
				  // skip string check as it is meaningless
				  // but there is not continue on angular foreach
//				  continue;
			  }
			  
			//----------------------
			// Check string (search1,searc2,..)
			if ( typeof($scope.searchString) === "undefined" )
			{
				matchString = true;
			}
			else
			{
				  var stmp;
				  var argCount;
				  
				  stmp = angular.lowercase( $scope.searchString );
				  stmp = stmp.split(',');
				  argCount = stmp.length;
		
				  while( argCount-- )
				  {
					  var tmp = stmp[argCount].replace(/^\s+|\s+$/g, '');
			
	//				  if ( argCount != 0 && tmp.length < 3 ) continue;
	
					  if (
							  angular.lowercase($scope.umowyInit[key].umowaOpis).indexOf(tmp) != -1 ||
							  angular.lowercase($scope.umowyInit[key].zlecenioBiorca).indexOf(tmp) != -1 ||
							  angular.lowercase($scope.umowyInit[key].umowa).indexOf(tmp) != -1 
						)
						{
						  matchString = true;
						  break;
						}
				  }
			  }
			
			  if ( matchString & matchDate )
			  {
				$scope.umowy[count] = $scope.umowyInit[key];
			  	count++
			  }
		    });
		  
		  $scope.updatePageCount();
		  $scope.updateTotalPrice();
	  };	  
	  
	//---------------------------------------------------------
	  $scope.updateTotalPrice = function()
	  {
		  if ($scope.umowy.length > 1000 )
		  {
			$scope.totalPrice = 0;
//			$scope.totalPriceClass = "ng-hide";
			return;
		  }
		  
		  var total;
		  total = 0;
		  
		  angular.forEach( $scope.umowy, function(value, key) {
			  total += value.cena;
		  });
		  $scope.totalPrice = total;
		  $scope.totalPriceClass = "totalPrice";
		  $scope.totalPriceShow = true;
	  };
	 
	  //---------------------------------------------------------------------------
	  $scope.quotesFix = function( str )
	  {
		  var ret;
		  ret = str.replace(/'/g, '"');
		  console.log("ret: "+ret);
		  return ret;
	  }
	  
	//---------------------------------------------------------
	   $scope.test = function( $fileCount ){
		  alert('test');
		  if ( $fileCount != null && $fileCount > 0 )
			  return "";
		  else
			  return "btn";
		  $scope.viewFilesButtonn = '<button>dupa</button>';
	  };
	  
  }]).$inject = ['$scope','notify'];

//  
//  
//  controller('Admin', ['$scope', '$http', function($scope, $http){
//	  window.location.href = 'http://cru.blackted.com:5566/admin/';
//  }]);