'use strict';

var service = angular.module('tagServiceModule', [] );
service.factory('tagServiceProvider', ['$http',  function(  $http ) {

	var myTags = function( cruId ) {
		this.cruId = cruId;
		this.tags = [];
		this.newTags = [];
		this.umowy = {};
		this.initTags = {};
		this.tmpUmowy = [];
		
		console.log("cruid " + cruId);
		$http({method:'JSONP', url: PHPBackend+'/getAllTags.php?cruId=' + this.cruId + '&callback=JSON_CALLBACK'}).
			success(function(data) 
		  	{
				this.tags = data;
	  	
				$http({method:'JSONP', url: PHPBackend+'/getTagsForAll.php?cruId=' + this.cruId + '&callback=JSON_CALLBACK'}).
				success(function(data) 
			  	{
					var myThis = this;

					angular.forEach(data, function(value, key)
					{
						var umowaId = value['umowa_id'];
						myThis.tmpUmowy[umowaId] = [];
					});
					
					angular.forEach(data, function(value, key)
					{
						var umowaId = value['umowa_id'];
						var tagId = value['tag_id'];
						var tagName = value['tag'];

						myThis.tmpUmowy[umowaId][tagName] = true;
				    });
			  	}.bind(this)).
			  	error(function(data, status, headers, config) 
			  	{
			  		console.log("error " +status );
				});
				
		  	}.bind(this)).
		  	error(function(data, status, headers, config) 
		  	{
		  		console.log("error " +status );
			});
	
	};
	 
	
//	myTags.prototype.checkboxInit = function( umowaId ) {
//		if ( typeof(this.tmpUmowy[umowaId] === "undefined"))
//		{
//			console.log("init from html");
//			this.tmpUmowy[umowaId] = [];
//		}
//	};
//	{ 'umowaId' :1, 'tags' : { 'tag': 'kosciol', 'id': 3} ,{ 'tag': 'kosciol', 'id': 3} }
	myTags.prototype.tagExists = function( tagName ) 
	{
		var ret;
		ret = false;
		
		angular.forEach(this.tags, function(value, key)
		{
			if ( value['tag'] == tagName )
				ret = true;
		});
		
		return ret;
	}
	
	//-----------------------------------------------------------------
	myTags.prototype.getTags = function( row ) 
	{
		var rowCount = 5;
		var tags;
		var start;
		var stop;
		var count;
		
//		count = Math.floor(this.tags.length / rowCount);
		count =5;
		
//		console.log("tags count "+this.tags.length+" " +count);
		
		start = 0 + row * count;
		stop = count + row * count;
		
		tags = this.tags.slice(start, stop);
		return tags;
	}
	//-----------------------------------------------------------------
	myTags.prototype.addTag = function() {
		
		if ( typeof(this.newTag) === "undefined" )
			return;
		
		if ( this.tagExists(this.newTag) )
		{
			alert("Ju\u017c istnieje");
			return;
		}
		
		var len;
		len = this.newTags.length;
		  
		this.newTags[this.newTags.length] 	= { 'tag' : this.newTag };
		this.tags[this.tags.length] 		= { 'tag' : this.newTag };
		  
		this.newTag = "";
	 };
	  
	  //-----------------------------------------------------------------
		myTags.prototype.checkTag = function( tag, umowa ) {
			
			var umowyTags = JSON.stringify(this.umowy);
			var newTags 	= JSON.stringify(this.newTags)
			var tags = {};
			var value;
			
			value = this.tmpUmowy[umowa.umowaId][tag.tag];
			  
			if (typeof(value) === "undefined" )
				value = false;
			  
			tags = { 'id': tag.id, 'tag' : tag.tag, 'value' : value };
			  
			if ( typeof(this.umowy[umowa.umowaId]) === "undefined" )
				this.umowy[umowa.umowaId] = {};
			  
			this.umowy[umowa.umowaId][tag.tag] = tags ;

			console.log(JSON.stringify(this.umowy));
			  //
		  };
		
	  //-----------------------------------------------------------------  
	  myTags.prototype.submit = function() {
		  var test;
		  
		  if (typeof(this.umowy) === "undefined")
			  return;
		  
		  var umowyTags = JSON.stringify(this.umowy);
		  var newTags 	= JSON.stringify(this.newTags)
		  
		  var data;
		  data = "cruId=" + this.cruId + "&";
		  data += "tags=" + JSON.stringify( this.umowy ) + "&";
		  data += "newTags=" + JSON.stringify( this.newTags );

		  
//		  var myUrl =  'http://dev.blackted.com/cru/raporty/app/php/api/submitTags.php?cruId=' + this.cruId + '&tags=' + umowyTags + '\'&newTags=' + newTags + '&callback=JSON_CALLBACK';
		  var myUrl =  'http://dev.blackted.com/cru/raporty/app/php/api/submitTags.php';
		  console.log(data);
		  $http({method:'PUT', data:data, url: myUrl }).
			success(function(data) 
		  	{
				console.log(data);
				console.log("submitted");
		  	}).
		  	error(function(data, status, headers, config) 
		  	{
		  		
		  		console.log("error " +status );
			});
	  }
	  
	return myTags;
}]);
  


