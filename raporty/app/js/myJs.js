'use strict';

var FrontendUrl = 'http://trup.blackted.com';
var BackendUrl = 'http://trup.blackted.com';
var PHPBackend = BackendUrl+'/json-api2/api';

var AdminUrl = 'http://trup.blackted.com';
var AdminPHPUrl = 'http://dev.blackted.com/cru/raporty/app/php/api/';

//FrontendUrl = 'http://192.168.3.2/cru/public/frontend/app';
//BackendUrl = 'http://192.168.3.2/cru/public/frontend/app';
//PHPBackend = BackendUrl+'/json-api2/api';

var Config = new Array();

Config[0] = new Array();
Config[0][0] = 'Urz\u0105d Miasta Szczecin';

Config[1] = new Array();
Config[1][0] = 'Urz\u0105d Miasta Milan\u00f3w';

Config[2] = new Array();
Config[2][0] = 'Ministerstwo Kultury i Dziedzictwa Narodowego';

Config[69] = new Array();
Config[69][0] = 'Testowy';



//----------------------------------------------------------------------
function puGetData( dataStr )
{
	var d;
	d = dataStr.split('-');
	return new Date(d[0], d[1], d[2]);
}
//----------------------------------------------------------------------

function puGetDataSafe( dataStr, fallbackDataStr )
{
	var d;

	if ( typeof(dataStr) === "undefined" )
	{
		d = fallbackDataStr.split('-');
		return new Date(d[0], d[1], d[2]);
	}
	
	return puGetData( dataStr );
}
//----------------------------------------------------------------------
function puCapitaliseFirstLetter(string)
{
    return string.charAt(0).toUpperCase() + string.slice(1);
}
