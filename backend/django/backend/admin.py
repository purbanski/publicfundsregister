from django.contrib import admin
from backend.models import Umowa
from backend.models import Plik
from backend.models import Komentarz
from backend.models import Tag
from django.template.response import TemplateResponse
from django.forms.extras.widgets import *
from django.forms import CheckboxSelectMultiple
from django import forms
from django.forms.extras.widgets import SelectDateWidget
from django.db import models
import os



# admin.site.register(Membership)
#---------------------------------------------------------------
#---------------------------------------------------------------
def sub_label(str, limit):
    ret = str[0:limit];
    if len(str) > limit:
        ret += '...'
    return ret

#---------------------------------------------------------------
#---------------------------------------------------------------
def umowa_get_opis(obj):
    return sub_label(obj.opis, 40)
 
#---------------------------------------------------------------
def umowa_get_firma(obj):
    return sub_label(obj.firma, 40)
 
#---------------------------------------------------------------
def umowa_get_data(obj):
    return obj.data
#---------------------------------------------------------------
def umowa_get_cena(obj):
    return obj.cena


# #---------------------------------------------------------------
umowa_get_opis.short_description = 'Opis'
umowa_get_firma.short_description = 'Firma'
umowa_get_cena.short_description = 'Cena'
umowa_get_data.short_description = 'Data'

class PlikInline(admin.TabularInline):
    model = Plik
    extra = 1

# 
# class UmowoTagInline(admin.TabularInline):
#     model = Tag.umowa.through
# #     model = Umowa
#     template = "admin/edit_inline/tabular2.html"
#     fieldsets = (
#         (None, {
#             'fields': ('Tag') }),
#     
#     ('Advanced options', {
#             'classes': ('collapse',),
#             'fields': ('enable_comments', 'registration_required', 'template_name')
#         }),)
#     formfield_for_manytomany = {
#         models.TextField: {'widget': forms.CheckboxSelectMultiple},
#     }
#     def formfield_for_dbfield(self, db_field, **kwargs):
# #         print db_field.name
#         if db_field.name == 'id':
#             kwargs['widget'] = CheckboxSelectMultiple
# #         kwargs['widget'] = (
# #                 ('accepted', 'Accepted'),
# #                 ('denied', 'Denied'),
# #             )
#         return super(UmowoTagInline,self).formfield_for_dbfield(db_field,**kwargs)
    
#     formfield_overrides = {
#         models.ManyToManyField: {'widget': forms.CheckboxSelectMultiple},
#     }
#     change_form_template = "admin/edit_inline/tabular2.html"

#     def get_osm_info(self):
#         # ...
#         pass
# 
#     def change_view(self, request, object_id, form_url='', extra_context=None):
#         extra_context = extra_context or {}
#         extra_context['osm_data'] = self.get_osm_info()
#         return super(UmowoTagInline, self).change_view(request, object_id,
#             form_url, extra_context=extra_context)
# # 
#     def formfield_for_manytomany(self, db_field, request, **kwargs):
#         if db_field.name == "umowa":
#             kwargs["queryset"] = Umowa.objects.all
#         return super(UmowaTagInLine, self).formfield_for_manytomany(db_field, request, **kwargs)

# 
# 
# class GroupAdmin(admin.ModelAdmin):
#     inlines = [
#         MembershipInline,
#     ]
#     exclude = ('members',)
# 
# admin.site.register(Group,GroupAdmin)
# admin.site.register(Person, PersonAdmin)

 
# class TagInline(admin.TabularInline):
#     model = Tag
#     extra = 1
       
class UmowaAdmin(admin.ModelAdmin):
    list_display = ('id', umowa_get_data, umowa_get_firma, umowa_get_opis, umowa_get_cena)
    list_filter = ['data']
    search_fields = ['id','umowa','firma','opis','cena']
    
#     def formfield_for_choice_field(self, db_field, request, **kwargs):
#         if db_field.name == "tags":
#             kwargs['choices'] = (
#                 ('accepted', 'Accepted'),
#                 ('denied', 'Denied'),
#             )
#             if request.user.is_superuser:
#                 kwargs['choices'] += (('ready', 'Ready for deployment'),)
#         return super(MyModelAdmin, self).formfield_for_choice_field(db_field, request, **kwargs)
    
    formfield_overrides = {
        models.ManyToManyField: {'widget': forms.CheckboxSelectMultiple},
    }
    
    fieldsets = [
                 ('Dane umowy', 
                  {'fields': (('umowa','data'),'firma', 'opis','cena', 'tags') } ) ]
    
    inlines = [PlikInline]
# 
#    change_form_template = "admin/change_form_umowy.html"
# # 
    def get_osm_info(self):
#         # ...
        pass
# # 
    def change_view(self, request, object_id, form_url='', extra_context=None):
        extra_context = extra_context or {}
        extra_context['osm_data'] = self.get_osm_info()
        return super(UmowaAdmin, self).change_view(request, object_id,
            form_url, extra_context=extra_context)

    
admin.site.register(Umowa,UmowaAdmin)



#---------------------------------------------------------------
#---------------------------------------------------------------
def plik_get_opis(obj):
    return sub_label(obj.opis,30)

#---------------------------------------------------------------
def plik_get_umowa(obj):
    return obj.umowa.umowa

#---------------------------------------------------------------
def plik_get_umowa_data(obj):
    return obj.umowa.data

#---------------------------------------------------------------
def plik_get_firma(obj):
    return sub_label(obj.umowa.firma, 100)

#---------------------------------------------------------------
def plik_get_umowa_opis(obj):
    return sub_label(obj.umowa.opis, 150)

#---------------------------------------------------------------
def plik_get_plik(obj):
    plik = str(obj.plik)
    ret = os.path.basename(plik)
    return ret

#---------------------------------------------------------------
def plik_get_umowa_cena(obj):
    return obj.umowa.cena

#---------------------------------------------------------------
#---------------------------------------------------------------
plik_get_opis.short_description = 'Opis pliku'
plik_get_firma.short_description = 'Zleceniobiorca'
plik_get_plik.short_description = 'Nazwa pliku'
plik_get_umowa.short_description = 'Umowa'
plik_get_umowa_data.short_description = 'Data zawarcia'
plik_get_umowa_opis.short_description = 'Opis umow'
plik_get_umowa_cena.short_description = 'Opis umow'

class PlikAdmin(admin.ModelAdmin):
    list_display = (
                    plik_get_plik, 
                    plik_get_opis, 
                    plik_get_umowa, 
                    plik_get_umowa_data, 
                    plik_get_firma, 
                    plik_get_umowa_opis, 
                    plik_get_umowa_cena)
    
admin.site.register(Plik,PlikAdmin)



#---------------------------------------------------------------
def tag_get_tag(obj):
    return obj.tag
#---------------------------------------------------------------
def tag_get_id(obj):
    return obj.id

#---------------------------------------------------------------
#---------------------------------------------------------------
tag_get_tag.short_description = 'Kategoria'
# plik_get_firma.short_description = 'Zleceniobiorca'
# plik_get_plik.short_description = 'Nazwa pliku'
# plik_get_umowa.short_description = 'Umowa'
# plik_get_umowa_data.short_description = 'Data zawarcia'
# plik_get_umowa_opis.short_description = 'Opis umow'
# plik_get_umowa_cena.short_description = 'Opis umow'
   
class TagAdmin(admin.ModelAdmin):
    list_display = (
                    tag_get_tag,
                    tag_get_id )
    
admin.site.register(Tag, TagAdmin)

admin.site.register(Komentarz)
