from django.db import models
from django.db.models import Count
import re
import os
     

class Tag(models.Model):
#     umowa       = models.ManyToManyField(Umowa)
    tag         = models.CharField(unique=True,max_length=64)
    opis        = models.TextField()
 
    def __str__(self):
        str = self.tag.encode('utf-8')
        return str
     
class Umowa(models.Model):
    umowa = models.CharField(unique=True,max_length=64)
    tags  = models.ManyToManyField(Tag)
    data  = models.DateField(verbose_name='Data zawarcia umowy')
    firma = models.CharField(verbose_name='Nazwa kontrahenta', max_length=128)
    opis  = models.TextField(verbose_name='Przedmiot umowy')
    cena  = models.DecimalField(verbose_name='Wartosc umowy brutto [PLN]', max_digits=32, decimal_places=2)

    def __str__(self):
        umowa = re.sub(r'(?is)CRU\/13\/0*', 'CRU: ', self.umowa)
        return "%s" % (umowa)
    
    @staticmethod
    def get_umowy_for_firma(aFirma):
        umowy = Umowa.objects.filter(firma=aFirma)
        return umowy
    
    @staticmethod
    def get_umowy_sorted(aSortBy='data', aSortType='desc' ):
        orderBy = ''
        if aSortType == 'desc':
            orderBy = '-'
 
        orderBy += aSortBy
        return Umowa.objects.all().order_by(orderBy)




class Plik(models.Model):
    baseDir = os.path.dirname(os.path.abspath(__file__))
    uploadDir = baseDir + '../../../../public/frontend/app/upload/'
    
    umowa   = models.ForeignKey(Umowa)
    opis    = models.CharField(max_length=128)
    plik    = models.FileField(upload_to=uploadDir)

    def __str__(self):
        return "%s" % (os.path.basename(str(self.plik)))
        
        
class Komentarz(models.Model):
    umowa       = models.ForeignKey(Umowa)
    nick        = models.CharField(max_length=128)
    komentarz   = models.TextField()
    data        = models.DateTimeField(verbose_name='Data wyslanai')
    ip          = models.IPAddressField()
 
    def __str__(self):
        return "%s" % (os.path.basename(str(self.umowa.umowa)))
     

class Wniosek(models.Model):
    umowa       = models.CharField(unique=True,max_length=64)
    data        = models.DateField(verbose_name='Data zawarcia umowy')
    wyslany     = models.BooleanField(default=False)
    wyslany_data= models.DateTimeField()
  
     