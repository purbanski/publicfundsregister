#!/bin/sh

FILE=php/config.php

sed -i '9,13d' ../public/frontend/app/json-api2/config.php
sed -i '6,9d' ../public/frontend/app/js/myJs.js

[ -f deploy.sh ] && rm -rf deploy.sh
