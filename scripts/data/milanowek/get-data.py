#!/usr/bin/python
# -*- coding: utf-8 -*-

import unicodedata
import re
import MySQLdb
import codecs
import sys

import unicodedata# from chardet import detect


db = MySQLdb.connect("localhost","root","dupadupa","CRU" )


#########################
def insert_szczecin_um_umowa(aUmowa, aData, aFirma, aOpis, aCena):
    cursor = db.cursor()
    aFirma = aFirma.replace('\'', '\\\'')
    aFirma = aFirma.replace('\n','')
    aFirma = aFirma.replace('\r','')
    aOpis = aOpis.replace('\'', '\\\'')
    sql = """ 
        INSERT INTO  CRU_MILANOWEK.backend_umowa ( umowa, data, firma, opis, cena)  
        VALUES ( '""" + aUmowa + "','" + aData + "','" + aFirma + "','" + aOpis + "','" + aCena + "');"
    
    skipped = False    

    try:
        skipped = False
        cursor.execute("SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'")
        cursor.execute(sql)
        db.commit()
    
    except:
        db.rollback
        print "Skipping ",  aUmowa
        skipped = True 

    if not skipped :
        print "Inserted ", aUmowa, " -----------"


def pol_fix(s):
    ret = re.sub(r'\\xc5\\x82','ł', s)
    ret = re.sub(r'\\xc4\\x99','ę', ret)
    ret = re.sub(r'\\xc3\\xb3','ó', ret)
    ret = re.sub(r'\\xc4\\x85','ą', ret)
    ret = re.sub(r'\\xc5\\xbc','ż', ret)
    ret = re.sub(r'\\xc5\\x81','Ł', ret)
    ret = re.sub(r'\\xc5\\x9b','ś', ret)
    ret = re.sub(r'\\xc5\\x9a','Ś', ret)
    ret = re.sub(r'\\xc4\\x87','ć', ret)
    ret = re.sub(r'\\xc5\\x84','ń', ret)
    ret = re.sub(r'\\xc5\\xbb','Ż', ret)
    ret = re.sub(r'\\xc5\\xba','ź', ret)
    ret = re.sub(r'\\xc5\\x83','Ń', ret)
    ret = re.sub(r'\\xc4\\x98','Ę', ret)
    ret = re.sub(r'\\xc3\\x93','Ó', ret)
    ret = re.sub(r'\\xc4\\x84','Ą', ret)
    ret = re.sub(r'\\xc5\\xb9','Ź', ret)
    ret = re.sub(r'\\xc4\\x86','Ć', ret)
    ret = re.sub(r'\\xc2\\xa8','"', ret)
#     ret = re.sub(r'\\xc4\\x84','Ą', ret)
    return ret

###########################
# import httplib2
# import requests
# import urllib2
# 
# 
# # print page.content
# # resp, content = httplib2.Http().request(szczecin_cru_url)
# # for row in resp:
# #     print row
# 
# # page = urllib2.urlopen(szczecin_cru_url).read()
# # for row in page:
# #     print row
# 
# szczecin_cru_url = 'http://bip.um.szczecin.pl/umszczecinbip/chapter_50829.asp'
# page = requests.get(szczecin_cru_url)



from HTMLParser import HTMLParser

class ProcState(object):
        IDLE        = 1
        PR_TR1      = 2
        PR_TD1      = 3 
        PR_TD2      = 4 
        PR_CRU_ID   = 5 
        PR_CRU_ID_DONE = 6 
        PR_TD_OPIS  = 7
        PR_OPIS     = 8
        PR_OPIS_DONE = 9
        PR_TD_FIRMA = 10
        PR_FIRMA = 11
        PR_FIRMA_DONE = 12
        PR_TR_CENA = 13
        PR_CENA = 14
        PR_CENA_DONE = 15
        
# create a subclass and override the handler methods
class MyHTMLParser(HTMLParser):
    
    state = ProcState.IDLE
    record = {}
    records = []
    recordsCount = 0
    
    def handle_starttag(self, tag, attrs):
#         print self.state
        
        if ( tag == 'tr' and self.state == ProcState.IDLE ) :
            self.state = ProcState.PR_TR1
            return
        
        if ( tag == 'td' and self.state == ProcState.PR_TR1 ) :
            self.state = ProcState.PR_TD1
            return
        
        if ( tag == 'td' and self.state == ProcState.PR_TD1 ) :
            self.state = ProcState.PR_TD2
            return
        
        if ( tag == 'span' and self.state == ProcState.PR_TD2 ) :
            self.state = ProcState.PR_CRU_ID
            return

        if ( tag == 'td' and self.state == ProcState.PR_CRU_ID_DONE ) :
            self.state = ProcState.PR_TD_OPIS
            return
        
        if ( tag == 'span' and self.state == ProcState.PR_TD_OPIS ) :
            self.state = ProcState.PR_OPIS
            return
        
        if ( tag == 'td' and self.state == ProcState.PR_OPIS_DONE ) :
            self.state = ProcState.PR_TD_FIRMA
            return
        
        if ( tag == 'span' and self.state == ProcState.PR_TD_FIRMA ) :
            self.state = ProcState.PR_FIRMA
            return

        if ( tag == 'tr' and self.state == ProcState.PR_FIRMA_DONE ) :
            self.state = ProcState.PR_TR_CENA
            return

        if ( tag == 'span' and self.state == ProcState.PR_TR_CENA ) :
            self.state = ProcState.PR_CENA
            return

    def handle_endtag(self, tag):
        if ( tag == 'tr' and self.state == ProcState.PR_CENA_DONE ) :
            self.records.append(self.record)
            self.record = {}
            self.state = ProcState.IDLE
        
    def handle_data(self, data):
        
        if ( self.state == ProcState.PR_CRU_ID ) :
            self.record[0] = data
            self.record[1] = ''
            
            self.state = ProcState.PR_CRU_ID_DONE
            return
        
        if ( self.state == ProcState.PR_OPIS ) :
            self.record[3] = data
            self.state = ProcState.PR_OPIS_DONE
            return

        if ( self.state == ProcState.PR_FIRMA ) :
            self.record[2] = data
            self.state = ProcState.PR_FIRMA_DONE
            return
    
        if ( self.state == ProcState.PR_CENA ) :
            self.record[4] = re.sub(r'[^0-9]','', data)
            if ( len(self.record[4]) == 0 ) :
                self.record[4] = 0
                
            self.state = ProcState.PR_CENA_DONE
            return
    
    def get_records(self) :
        return self.records
        

# instantiate the parser and fed it some HTML


parser = MyHTMLParser()
f = codecs.open("blog-data.html","r")

txt = ''
for line in f:
    txt = txt + line

# print txt

parser.feed(txt)
# '<html><head><title>Test</title></head>'
#             '<body><h1>Parse me!</h1></body></html>')


# sys.exit()

# records = get_szczecin_um_records(f)
records = parser.get_records()

# print get_szczecin_um_records()
# for row in records:
for i, val in enumerate(records):
#     array[i] += 1
#     
# if ( 1 ==  1) :
    data = records[i]
#     print (data)
    if data.has_key(0):
        id      = pol_fix(data[0])
        umowa   = pol_fix(data[1])
        firma   = pol_fix(data[2])
        opis    = pol_fix(data[3])
        cena    = str(data[4])

#         print "id:" +id + "\tumowa:" + umowa + "\tfirma:" + firma + "\topis:" + opis + "\tcena:" + cena
#         print "id:" +id + "\tumowa:" + umowa + "\tfirma:" + firma  + "\topis:" + opis + "\tcena:" + cena
#         print cena
#         print "\n"
        insert_szczecin_um_umowa(id, umowa, firma, opis, cena )

db.close()

