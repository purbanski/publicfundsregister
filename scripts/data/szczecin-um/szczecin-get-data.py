#!/usr/bin/python
# -*- coding: utf-8 -*-

import unicodedata
import re
import MySQLdb
import codecs

import unicodedata# from chardet import detect

# db_name = "TRUP_Test";
db_name = "TRUP_Szczecin_UM";

db = MySQLdb.connect("localhost","trup","6YLPVtUMJbDKfTBX", db_name )

#########################
def set_sent_table(aUmowa, aData):
    cursor = db.cursor()

#     sql = """
#         SELECT id FROM """ + db_name + """.backend_umowa 
#         WHERE umowa =  '""" + aUmowa + "';"
# 
#     skipped = False
#     cursor.execute("SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'")
#     cursor.execute(sql)
#     db.commit()
#     
#     skipped = False
#     try :
#        umowaId = cursor.fetchone()[0]
#     
#     except :
#         print "Problem with " + aUmowa
#         return
    
    sql = """ 
        INSERT INTO  """ + db_name + """.backend_wniosek ( umowa, data, wyslany,  wyslany_data )
        VALUES ( '""" + aUmowa  + "','" + aData + "', 0,  '0000-00-00 00:00:00' ); """

    skipped = False
    
    try :
        cursor.execute("SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'")
        cursor.execute(sql)
        db.commit()
    except:
        db.rollback
        skipped = True
        print "Skipped Wniosek " + aUmowa 

    if not skipped :
        print "Inserted Wniosek ", aUmowa, " -----------"

#########################
def insert_szczecin_um_umowa(aUmowa, aData, aFirma, aOpis, aCena):
    cursor = db.cursor()
    aFirma = aFirma.replace('\\\'', '\'')
    aFirma = aFirma.replace('\'', '\\\'')
    aFirma = aFirma.replace('\n','')
    aFirma = aFirma.replace('\r','')
   
    aOpis = aOpis.replace('\\\'', '\'') # \' -> '    -necessary to work with both \' and  ' as input
    aOpis = aOpis.replace('\'', '\\\'') # '  -> \'
    
    sql = """ 
        INSERT INTO  """ + db_name + """.backend_umowa ( umowa, data, firma, opis, cena)  
        VALUES ( '""" + aUmowa + "','" + aData + "','" + aFirma + "','" + aOpis + "','" + aCena + "');"
    
    skipped = False    

    try:
        skipped = False
        cursor.execute("SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'")
        cursor.execute(sql)
        db.commit()
    except:
        db.rollback
        print "Skipping ",  aUmowa
        skipped = True 

    if not skipped :
        print "Inserted ", aUmowa, " -----------"
        set_sent_table(aUmowa, aData)
    
# parses html file and returns dictionary
def get_szczecin_um_records(htmlContent):
#     f = open("UM_Szczecin_26-12-2013.html","r") #opens file with name of "test.txt"
#     encoding = lambda x: detect(x)['encoding']
#     for line in f:
#         print encoding(line)

    class ProcState(object):
        IDLE = 1
        PROCESSING_STARTED = 2
        PROCESSING_STOPPED = 3
        PROCESSING_ENTRY = 4
        PROCESSING_IDLE = 5
    
    show = False
    startPattern = '<div id="content">'
    endPattern = '</tbody>'
    startEntryPattern  = '<tr>'
    endEntryPattern = '</tr>'
    startCellPattern  = '<td>'
    endCellPattern = '</td>'
    
    procState = ProcState.IDLE
    record = ''
    rowCount = 0
    records = {}
    
    for line in htmlContent:
        # Checking for first start
        if procState == ProcState.IDLE and startPattern in line:
            procState = ProcState.PROCESSING_STARTED
            
        # Checking for entry processing / looking for <tr>
        if procState == ProcState.PROCESSING_STARTED or procState == ProcState.PROCESSING_IDLE :
            if startEntryPattern in line:
                procState = ProcState.PROCESSING_ENTRY
            
        # Checking for entry processing / looking for <tr>
        if procState != ProcState.PROCESSING_STOPPED :
            if endPattern in line:
                procState = ProcState.PROCESSING_STOPPED
    
        # Composing record
        if procState == ProcState.PROCESSING_ENTRY :
             record += line.replace('\n','')
            
        if procState == ProcState.PROCESSING_ENTRY:
            if endEntryPattern in line :
#                 print "----------"
                reclist = []
                reclist = record.split('</td>')
#                 print reclist.length
#                 tmp = re.sub(r'<tr>.*<td>', r'', reclist[0])
#                 print tmp 
                
                finalRecord = {}
                finalRecord[0] = re.sub(r'<tr>.*<td>', r'', reclist[0])
                finalRecord[1] = re.sub(r'.*<td>([0-9]{4})/([0-9]{1,2})/([0-9]{1,2}).*', r'\1/\2/\3', str(reclist[1:2]))
                finalRecord[2] = re.sub(r'.*<td>(.*)\'\]$', r'\1', str(reclist[2:3]))
                finalRecord[3] = re.sub(r'.*<td>(.*)\'\]$', r'\1', str(reclist[3:4]))
                finalRecord[4] = re.sub(r'.*<td>(.*)\'\]$', r'\1', str(reclist[4:5]))
                
                records[rowCount] = finalRecord
                rowCount += 1
                    
                    
                record =''
                procState = ProcState.PROCESSING_IDLE
    records[0] ={}
    return records

def pol_fix(s):
    ret = re.sub(r'\\xc5\\x82','ł', s)
    ret = re.sub(r'\\xc4\\x99','ę', ret)
    ret = re.sub(r'\\xc3\\xb3','ó', ret)
    ret = re.sub(r'\\xc4\\x85','ą', ret)
    ret = re.sub(r'\\xc5\\xbc','ż', ret)
    ret = re.sub(r'\\xc5\\x81','Ł', ret)
    ret = re.sub(r'\\xc5\\x9b','ś', ret)
    ret = re.sub(r'\\xc5\\x9a','Ś', ret)
    ret = re.sub(r'\\xc4\\x87','ć', ret)
    ret = re.sub(r'\\xc5\\x84','ń', ret)
    ret = re.sub(r'\\xc5\\xbb','Ż', ret)
    ret = re.sub(r'\\xc5\\xba','ź', ret)
    ret = re.sub(r'\\xc5\\x83','Ń', ret)
    ret = re.sub(r'\\xc4\\x98','Ę', ret)
    ret = re.sub(r'\\xc3\\x93','Ó', ret)
    ret = re.sub(r'\\xc4\\x84','Ą', ret)
    ret = re.sub(r'\\xc5\\xb9','Ź', ret)
    ret = re.sub(r'\\xc4\\x86','Ć', ret)
    ret = re.sub(r'\\xc2\\xa8','"', ret)
#     ret = re.sub(r'\\xc4\\x84','Ą', ret)
    return ret

###########################
# import httplib2
# import requests
# import urllib2
# 
# 
# # print page.content
# # resp, content = httplib2.Http().request(szczecin_cru_url)
# # for row in resp:
# #     print row
# 
# # page = urllib2.urlopen(szczecin_cru_url).read()
# # for row in page:
# #     print row
# 
# szczecin_cru_url = 'http://bip.um.szczecin.pl/umszczecinbip/chapter_50829.asp'
# page = requests.get(szczecin_cru_url)

f = codecs.open("chapter_50829.asp","r")


records = get_szczecin_um_records(f)
# print get_szczecin_um_records()
for row in records:
    data = records[row]
#     print (data)
    if data.has_key(0):
        id      = pol_fix(data[0])
        umowa   = pol_fix(data[1])
        firma   = pol_fix(data[2])
        opis    = pol_fix(data[3])
        cena    = data[4]

#         print "id:" +id + "\tumowa:" + umowa + "\tfirma:" + firma + "\topis:" + opis + "\tcena:" + cena
#         print "\n"
        insert_szczecin_um_umowa(id, umowa, firma, opis, cena )

db.close()

