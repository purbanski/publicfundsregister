#!/bin/sh

#DIR='/home/sowp/cru/scripts/data/szczecin-um'
DIR='/var/www/virtuals/trup/scripts/data/szczecin-um/'

pushd $DIR &> /dev/null

LOG_DIR="import-`date +"%d%m%Y-%H%M"`"
LOG_FILE="$LOG_DIR/import.log"
DATA_FILE='chapter_50829.asp'

mkdir -p $LOG_DIR


echo "-----------------" >> $LOG_FILE
echo `date` >> $LOG_FILE
echo "-----------------" >> $LOG_FILE

./fileget.sh  2> $LOG_FILE  && ./szczecin-get-data.py >> $LOG_FILE

mv $DATA_FILE $LOG_DIR

popd &> /dev/null

