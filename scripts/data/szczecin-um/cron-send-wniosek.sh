#!/bin/sh

DIR='/var/www/html/cru/scripts/data/szczecin-um/'

pushd $DIR &> /dev/null

LOG_DIR="wniosek-`date +"%d%m%Y-%H%M"`"
LOG_FILE="$LOG_DIR/wniosek.log"

mkdir -p $LOG_DIR


echo "-----------------" >> $LOG_FILE
echo `date` >> $LOG_FILE
echo "-----------------" >> $LOG_FILE

./send-wniosek.py &> $LOG_FILE

popd &> /dev/null

