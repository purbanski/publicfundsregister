#!/usr/bin/python
# -*- coding: utf-8 -*-

import unicodedata
import sys
import re
import MySQLdb
import codecs
import unicodedata# from chardet import detect
import smtplib
from email.mime.text import MIMEText

############################################################
############################################################
## Get wnioski from sql
db_name = "TRUP_Szczecin_UM";
db = MySQLdb.connect("localhost","root","dupadupa", db_name)
records = {}

cursor = db.cursor()
sql = """ 
        SELECT * FROM  """ + db_name + """.backend_wniosek
        WHERE wyslany=0 AND data >= '2014-02-05'
        ORDER BY data ASC;"""
# print sql
cursor.execute("SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'")
cursor.execute(sql)
db.commit()

count = 0
umowyMsgUmowy = "";
while True:
    row = cursor.fetchone()
    if row == None :
        break
    
#     print row
    umowyMsgUmowy += "- " + row[1] + ", " + str(row[2]) + ";\r\n"
    records[count] = row
    count = count + 1
    
if count == 0 :
    print "Nie ma umw do wyslania"
    sys.exit(1)

umowyMsgUmowy +="\r\n"    

############################################################
############################################################
## Send mail
fromMail = "szymon.osowski@siecobywatelska.pl"
# fromMail = "purbanski@interia.pl"
# toShowMail = "purbanski@interia.pl"
# toShowMail = "przemek@blackted.com"
toShowMail = "bp@um.szczecin.pl"
ccShowMail = "rejestrumow@siecobywatelska.pl"

# fromMail = 
toMail = [ toShowMail, ccShowMail, "purbanski@interia.pl", "szymon.osowski@siecobywatelska.pl" ]

personMail = "Szymon Osowski"
subjectMail = "Wniosek o udostępnienie kopii umów"
umowyMsgHeader = "Wnoszę o udostępnienie skanów następujących umów (wraz z załącznikami tj. faktury, protokoły odbioru, aneksy itp):\r\n\r\n"
umowyMsgFooter = "Umowy proszę przesłać pocztą elektroniczną na adres: " + fromMail
umowyMsgFooter += "\r\n\r\nZ wyrazami szacunku,\r\n" + personMail + "\r\n"

    
mailTxt = umowyMsgHeader + umowyMsgUmowy + umowyMsgFooter    

msg = MIMEText(mailTxt, _charset="UTF-8")
msg['Subject'] =  subjectMail #'Header(subjectMail, "utf-8")'
msg['From'] = fromMail
msg['To'] = toShowMail
msg['Cc'] = ccShowMail

# Send the message via our own SMTP server, but don't include the
s = smtplib.SMTP('localhost')
s.sendmail(fromMail, toMail, msg.as_string())
s.quit()

print mailTxt
############################################################
############################################################
## Update

# sys.exit(1)

for row in records:
    r = records[row]
    sql = """ 
        UPDATE  """ + db_name + """.backend_wniosek 
        SET  wyslany =  '1' 
        WHERE """ + db_name + """.backend_wniosek.umowa = '""" + r[1] + "'"

    try :
        cursor.execute("SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'")
        cursor.execute(sql)
        db.commit()
    except :
        print r[1] + " update problem"
        
db.close()

