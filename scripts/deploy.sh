#!/bin/sh
set -e

git reset --hard
git pull origin master

db_user='sowp_trup'
db_pass='3l4nY1Tt'
db_name='sowp_trup'
domain='trup.jawnosc.tk'

INSTALL_DIR=/home/sowp/cru/

files="$INSTALL_DIR/scripts/data/szczecin-um/szczecin-get-data.py $INSTALL_DIR/public/frontend/app/json-api/api/getAllFilesForRecord.php $INSTALL_DIR/public/frontend/app/json-api/api/getAllRecords.php $INSTALL_DIR/public/frontend/app/json-api/api/getAllRecordsWithFiles.php $INSTALL_DIR/public/frontend/app/json-api/config.php $INSTALL_DIR/public/frontend/app/js/controllers.js $INSTALL_DIR/backend/django/cru/settings.py $INSTALL_DIR/public/frontend/app/json-api/api/postCommentForUmowa.php $INSTALL_DIR/public/frontend/app/json-api/api/getCommentsForUmowa.php $INSTALL_DIR/public/frontend/app/partials/umowy-tabelka.html"

for file in $files; do  
   ls -l $file
   sed -i -e "s/root/$db_user/g" -e "s/dupadupa/$db_pass/g" -e "s/CRU\./$db_name./g" -e "s/cru\.blackted\.com/$domain/g" $file
done

files="$INSTALL_DIR/backend/django/cru/settings.py $INSTALL_DIR/scripts/data/szczecin-um/szczecin-get-data.py $INSTALL_DIR/public/frontend/app/json-api/api/postCommentForUmowa.php $INSTALL_DIR/public/frontend/app/json-api/api/getCommentsForUmowa.php"
for file in $files; do
   sed -i -e "s/CRU/$db_name/g" $file
done

sed -i -e "s/^MEDIA_ROOT.*=.*$/MEDIA_ROOT = \'\/home\/sowp\/cru\/public\/frontend\/app\/upload\'/" $INSTALL_DIR/backend/django/cru/settings.py

pushd $INSTALL_DIR/backend/django
python manage.py syncdb
popd

$INSTALL_DIR/scripts/gunicorn.sh restart

