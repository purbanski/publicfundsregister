#!/bin/sh

CRU_DJANGO_DIR=~/cru/backend/django/
LISTEN_IP=0.0.0.0
LISTEN_PORT=5566

usage() 
{
	echo 
	echo "Uruchom: $0 [start | stop | restart]"
	echo
}

check_if_started()
{
	ps aux | grep -q "\/usr\/bin\/python \/home\/sowp\/\.local\/bin\/gunicorn"
}

if [ $# -ne 1 ]; then
 	usage
	exit 1
fi

gunicorn_start()
{
	gunicorn --workers=4  -b $LISTEN_IP:$LISTEN_PORT cru.wsgi:application -D
}

echo ""

case "$1" in

start)
	echo "Startuje gunicorn-a"
	check_if_started
	if [ $? -eq 0 ]; then
		echo "Gunicorn juz dziala, nie startuje ponownie"
		exit 1
	fi
	pushd $CRU_DJANGO_DIR &> /dev/null
	gunicorn_start
	popd &> /dev/null
	sleep 1

	check_if_started
	if [ $? -eq 0 ]; then
		echo "wystartowany"
	else
		echo "problem ze startem..."
	fi
   ;;

stop) 
	echo  "Zatrzymuje gunicorn-a"
	
	check_if_started
	if [ $? -eq 1 ]; then
		echo "Gunicorn nie jest uruchomiony..."
		exit 1
	fi
	killall gunicorn
	sleep 3
	
	check_if_started
   if [ $? -eq 0 ]; then
 		echo "problem z zatrzymaniem.."
 	else
 		echo "zatrzymany..."
 	fi

    ;;

restart)	
	echo "zatrzumuje gunicorn-a"
	killall gunicorn
	sleep 3
	
	echo "startuje gunicorn-a"
	pushd $CRU_DJANGO_DIR &> /dev/null
	gunicorn_start
	popd &> /dev/null
	;;
*) 
	usage
	;;
esac


