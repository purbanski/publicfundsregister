# Reasoning #

***

In Poland according to Polish Constitution - receipts, agreements, documentation etc regarding expenses which were incurred by Council need to be available to polish residents upon request. Unfortunately many Councils are resistant to this law. Therefore [NGO Watchdog Poland][siec-obywatelska] ordered the web application, which server the purpose of register for all expenses incurred by any Council. 

## Public Funds Register ##

This applications is capable of storing expenses accross many different Councils.

## Technology Used ##

* Backend - Django
* Frontend - Angular 1.x

[siec-obywatelska]: http://siecobywatelska.pl/?lang=en