<?php 
require_once "../config.php";
require_once "mylib.php";

//----------------------------------------------------------------
function myGetText()
{
	$data = puGetData();
	$search = $data['search'];
	$ret = "";
	
	if (strlen( $search ) > 1 )
	{
		$ret .= " Szukana fraza: " . $search. ".";
	}
	
	if (strlen( $data['dateFrom'] ) > 1 )
	{
		$ret .= " Data od: " . $data['dateFrom'] . ".";
	}

	if (strlen( $data['dateTo'] ) > 1 )
	{
		$ret .= " Data do: " . $data['dateTo'] . ".";
	}
	return $ret;
}
//----------------------------------------------------------------

puSetConfig();

?>	
<!doctype html>
<html lang="en">

<head prefix="og: http://ogp.me/ns# product: http://ogp.me/ns/product#">
	<title>Totalny Rejestr Um&#243;w Publicznych - <?php echo Config::get('TrupName'); ?></title>

	<meta property="fb:app_id" content="801715806509923" />
	<meta property="og:image" content="http://trup.blackted.com/img/logo2.png"/>
 	<meta property="og:type" content="website"/>

 	<meta property="og:title" content="TRUP - <?php echo Config::get('TrupName'); ?>"/>
    <meta property="og:site_name" content="Totalny Rejestr Um&#243;w Publicznych, <?php echo Config::get('TrupName'); ?>"/>
	<meta property="og:description" content="Totalny Rejestr Um&#243;w Publicznych, <?php echo Config::get('TrupNameLong'); echo ". "; echo myGetText(); ?>"/> 
		
	<meta http-equiv="refresh" content="1; url=<?php echo puGetSearchUrl(); ?>" />
</head>

<body>
</body>
</html>