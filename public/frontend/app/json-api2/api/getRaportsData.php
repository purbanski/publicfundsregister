<?php 
//------------------------------------------------------------------------------
require_once '../config.php';
require_once '../toolbox.php';
//------------------------------------------------------------------------------
SetConfigFromVar();

$dateStart 	= $_GET['dateStart'];
$dateStop  	= $_GET['dateStop'];

if ( !isset($dateStop) || !isset($dateStart))
	die("Args");

$sql = "
		SELECT UT.firma, UT.opis, UT.data, UT.cena, UT.umowa_id, UT.tag_id, T.tag FROM
		(
 			SELECT UM.firma, UM.data, UM.cena, UM.opis, UM.id as umowa_id, tag_id
			FROM ". Config::get('DBName') .".backend_umowa_tags as IUT
			RIGHT JOIN ". Config::get('DBName') .".backend_umowa as UM
			ON UM.id = IUT.umowa_id
		) AS UT
		LEFT JOIN ". Config::get('DBName') .".backend_tag AS T
		ON UT.tag_id = T.id
		WHERE UT.data >='". $dateStart ."' AND UT.data <= '". $dateStop . "'
		ORDER BY tag_id ASC;";
// print $sql;
// ---
$con = mysql_connect(Config::get('DBServer'), Config::get('DBUsername'), Config::get('DBPassword'));
if (!$con)
{
	die('Could not connect: ' . mysql_error());
}
mysql_query("SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'", $con);
//---

$result = mysql_query($sql,$con);
$callback = $_GET['callback'];

echo $callback.'('.pu_mysql_to_json($result).');';
mysql_close($con);
//------------------------------------------------------------------------------
?>