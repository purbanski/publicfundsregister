<?php 

//------------------------------------------------------------------------------
require_once '../config.php';
require_once '../toolbox.php';
//------------------------------------------------------------------------------
SetConfigFromVar();

$sql = "
	SELECT 
		count(*)	AS count,
		firma		AS firma,
		sum(cena)	AS cena
	FROM ". Config::get('DBName') .".backend_umowa 
	GROUP BY firma  ORDER BY count(*)  DESC;";

//------------------------------------------------------------------------------
$con = mysql_connect(Config::get('DBServer'), Config::get('DBUsername'), Config::get('DBPassword'));
if (!$con)
{
	die('Could not connect: ' . mysql_error());
}
mysql_query("SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'", $con);
//------------------------------------------------------------------------------

$result = mysql_query($sql,$con);
$callback = $_GET['callback'];

echo $callback.'('.pu_mysql_to_json($result).');';
mysql_close($con);
//------------------------------------------------------------------------------
?>
