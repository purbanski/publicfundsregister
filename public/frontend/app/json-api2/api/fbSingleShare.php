<?php 
require_once "../config.php";
require_once "mylib.php";
require_once '../toolbox.php';


//----------------------------------------------------------------

puSetConfig();

$data = puGetData();
$cruId = $data['cruId'];
$umowaId = $data['umowaId'];

// print $cruId;

if ( !isset($cruId) || !is_numeric($cruId))
	die("");

if ( !isset($umowaId) || !is_numeric($umowaId))
	die("");

SetConfig($cruId);

$sql = "
	SELECT
		id 	AS umowaId,
		umowa 	AS umowa,
		data 	AS data,
		firma 	AS zlecenioBiorca,
		cena 	AS cena,
		opis 	AS umowaOpis
	FROM ". Config::get('DBName') .".backend_umowa umowaTab
	WHERE id = ". $umowaId .";";


//---
$con = mysql_connect(Config::get('DBServer'), Config::get('DBUsername'), Config::get('DBPassword'));
if (!$con)
{
	die('Could not connect: ' . mysql_error());
}
mysql_query("SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'", $con);
//---

$result = mysql_query($sql,$con);
$row = mysql_fetch_assoc($result); 
// var_dump($row);

mysql_close($con);

//-------------------
// Description
$umowa = $row['umowa'];
$umowa = explode('/', $umowa);

$zleceniobiorca = $row['zlecenioBiorca'];
$cena = $row['cena'];
$opis = $row['umowaOpis'];

// var_dump($umowa);

$desc = "Firma: ".$zleceniobiorca.". Cena: ".$cena." PLN. Opis: ".pu_fix_quotes($opis);
//----------------------------------------------------------------
// 	$ret = 'http://localhost:8000/app/index.html#';
$searchUrl = 'http://trup.blackted.com/#';
$searchUrl .= "/single_view";
$searchUrl .= "/".$cruId ;
$searchUrl .= "/".$umowa[0] ;
$searchUrl .= "/".$umowa[1] ;
$searchUrl .= "/".$umowa[2] ;

?>	
<!doctype html>
<html lang="en">

<head prefix="og: http://ogp.me/ns# product: http://ogp.me/ns/product#">
	<title>Totalny Rejestr Um&#243;w Publicznych - <?php echo Config::get('TrupName'); ?></title>

	<meta property="fb:app_id" content="801715806509923" />
	<meta property="og:image" content="http://trup.blackted.com/img/logo2.png"/>
 	<meta property="og:type" content="website"/>

 	<meta property="og:title" content="TRUP - <?php echo Config::get('TrupName'); ?>"/>
    <meta property="og:site_name" content="Totalny Rejestr Um&#243;w Publicznych, <?php echo Config::get('TrupName'); ?>"/>
	<meta property="og:description" content='<?php echo $desc; ?>' /> 
	<meta http-equiv="refresh" content="1; url=<?php echo $searchUrl; ?>" />
</head>
<body>
</body>
</html>