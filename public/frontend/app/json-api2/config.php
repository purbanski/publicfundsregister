<?php 
//----------------------------------------------------------------
date_default_timezone_set('Europe/Berlin');

$_defaultDbServer 		= 'localhost:3307';
$_defaultDbUser			= 'trup';
$_defaultDbPassword		= '6YLPVtUMJbDKfTBX';
$_defaultFrontendUrl	= 'http://trup.blackted.com';

$_defaultDbServer 		= 'localhost';
$_defaultDbUser			= 'root';
$_defaultDbPassword		= 'dupadupa';
$_defaultFrontendUrl	= 'http://trupdev.blackted.com';

$cfg['FrontendUrl'] 	= $_defaultFrontendUrl;
$cfg['Timestamp'] 		= date('[Y:m:d][H:i:s]') ."\n";
$cfg['ClientIP']		= $_SERVER['REMOTE_ADDR'];

$cfg[0]['DBServer'] 	= $_defaultDbServer;
$cfg[0]['DBUsername'] 	= $_defaultDbUser;
$cfg[0]['DBPassword'] 	= $_defaultDbPassword;
$cfg[0]['DBName'] 		= 'TRUP_Szczecin_UM';
$cfg[0]['TrupName']		= 'Szczecin';
$cfg[0]['TrupNameLong']	= 'Urz&#261;d Miasta Szczecin';

$cfg[1]['DBServer'] 	= $_defaultDbServer;
$cfg[1]['DBUsername'] 	= $_defaultDbUser;
$cfg[1]['DBPassword'] 	= $_defaultDbPassword;
$cfg[1]['DBName'] 		= 'TRUP_Milanwow_UM';
$cfg[1]['TrupName']		= 'Milan&#243;w';
$cfg[1]['TrupNameLong']	= 'Urz&#261;d Miasta Milan&#243;w';

$cfg[2]['DBServer'] 	= $_defaultDbServer;
$cfg[2]['DBUsername'] 	= $_defaultDbUser;
$cfg[2]['DBPassword'] 	= $_defaultDbPassword;
$cfg[2]['DBName'] 		= 'TRUP_mkidn';
$cfg[2]['TrupName']		= 'Ministerstwo Kultury';
$cfg[2]['TrupNameLong']	= 'Ministerstwo Kultury i Dziedzictwa Narodowego';

$cfg[3]['DBServer'] 	= $_defaultDbServer;
$cfg[3]['DBUsername'] 	= $_defaultDbUser;
$cfg[3]['DBPassword'] 	= $_defaultDbPassword;
$cfg[3]['DBName'] 		= 'TRUP_umwz';
$cfg[3]['TrupName']		= 'Urz&#261;d Marsza&#322;kowski';
$cfg[3]['TrupNameLong']	= 'Urz&#261;d Marsza&#322;kowski Wojew&#243;dzwta Zachodniopomorskiego';

$cfg[4]['DBServer'] 	= $_defaultDbServer;
$cfg[4]['DBUsername'] 	= $_defaultDbUser;
$cfg[4]['DBPassword'] 	= $_defaultDbPassword;
$cfg[4]['DBName'] 		= 'TRUP_Olawa_UM';
$cfg[4]['TrupName']		= 'O&#322;awa';
$cfg[4]['TrupNameLong']	= 'Urz&#261;d Miasta O&#322;awa';

$cfg[5]['DBServer'] 	= $_defaultDbServer;
$cfg[5]['DBUsername'] 	= $_defaultDbUser;
$cfg[5]['DBPassword'] 	= $_defaultDbPassword;
$cfg[5]['DBName'] 		= 'TRUP_Kety_UG';
$cfg[5]['TrupName']		= 'K&#281;ty';
$cfg[5]['TrupNameLong']	= 'Urz&#261;d Gminy K&#281;ty';

$cfg[69]['DBServer'] 	= $_defaultDbServer;
$cfg[69]['DBUsername'] 	= $_defaultDbUser;
$cfg[69]['DBPassword'] 	= $_defaultDbPassword;
$cfg[69]['DBName'] 		= 'TRUP_Test';
$cfg[69]['TrupName']	= 'TEST';
$cfg[69]['TrupNameLong']= 'Trup Testowy';
//----------------------------------------------------------------
Class Config
{
	static protected $config = array();
	private function __construct() {} //make this private so we can't instanciate
	public static function set($key, $val)
	{
		Config::$config[ $key ] = $val;
	}

	public static function get($key)
	{
		if ( !isset(Config::$config[ $key ]))
			return "";
		
		return Config::$config[ $key ];
	}
	
	public static function dump()
	{
		foreach( Config::$config as $key => $value )
		{
			echo "".$key."   = ". $value ."<br/>";
		}
	}
}
//----------------------------------------------------------------
function SetConfig( $cruId )
{
	global $cfg;

	if (!is_numeric($cruId))
		die();
	
	foreach( $cfg[$cruId] as $key => $value )
	{
// 		echo "".$key."   = ". $value ."<br/>";
 		Config::set($key, $value);	
	}	
}
//----------------------------------------------------------------
function SetConfigFromVar()
{
	$id = $_GET['cruId'];
	if (!is_numeric($id))
		die();
	SetConfig( $id );
}
	

?>
