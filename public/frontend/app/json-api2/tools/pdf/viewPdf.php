<!doctype html>
<html>

<head>
  <!-- In production, only one script (pdf.js) is necessary -->
  <!-- In production, change the content of PDFJS.workerSrc below -->
  <script type="text/javascript" src="pdf.js/src/shared/util.js"></script>
  <script type="text/javascript" src="pdf.js/src/shared/colorspace.js"></script>
  <script type="text/javascript" src="pdf.js/src/shared/pattern.js"></script>
  <script type="text/javascript" src="pdf.js/src/shared/function.js"></script>
  <script type="text/javascript" src="pdf.js/src/shared/annotation.js"></script>
  <script type="text/javascript" src="pdf.js/src/display/api.js"></script>
  <script type="text/javascript" src="pdf.js/src/display/metadata.js"></script>
  <script type="text/javascript" src="pdf.js/src/display/canvas.js"></script>
  <script type="text/javascript" src="pdf.js/src/display/font_loader.js"></script>

  <script type="text/javascript">
    // Specify the main script used to create a new PDF.JS web worker.
    // In production, leave this undefined or change it to point to the
    // combined `pdf.worker.js` file.
    PDFJS.workerSrc = 'pdf.js/src/worker_loader.js';
  </script>
</head>

<body>
  <canvas id="the-canvas" style="border:1px solid black;"/>
  
  <script type="text/javascript">
  'use strict';

  PDFJS.getDocument(<?php 
  
  //------------------------------------------------------------------------------
  require_once '../../config.php';
  require_once '../../toolbox.php';
  //------------------------------------------------------------------------------
  
  if ( !isset($_GET['f']) )
  	die('missing argument');
  
  $dir = '/backend/django/upload/';
  $file = $_GET['f'];
  
  pu_check_for_hacking( $file );
  $file = $dir.$file;

 echo '"'.$file.'"';

  ?>).then(function(pdf) {
    // Using promise to fetch the page
    pdf.getPage(1).then(function(page) {
      var scale = 1.5;
      var viewport = page.getViewport(scale);

      //
      // Prepare canvas using PDF page dimensions
      //
      var canvas = document.getElementById('the-canvas');
      var context = canvas.getContext('2d');
      canvas.height = viewport.height;
      canvas.width = viewport.width;

      //
      // Render PDF page into canvas context
      //
      var renderContext = {
        canvasContext: context,
        viewport: viewport
      };
      page.render(renderContext);
    });
  });

  </script>
</body>

</html>
