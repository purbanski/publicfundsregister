angular.module('myApp.controllers').

filter('reverse', function() {
	  return function(items) {
	    return items.slice().reverse();
	  };
	}).
directive('raportsGeneratedDirective', function() {
	  return function(scope, element, attrs) {
	    if (scope.$last){
	    	jQuery("#" + attrs['myid']).hide();
	    }
	  };
	}).

	controller('Raports', ['$scope', '$sce', '$http', '$routeParams', function($scope, $sce, $http, $routeParams )
	{
		//------------------------------------------------------------------------
		function ResetDaysLabel( skipReset )
		{
			if ( ! skipReset )
				$scope.buttonMonthsStyle 	= {};
			
			$scope.buttonDaysStyle 		= {}
			$scope.buttonDaysStyle['color'] = '#888888';

			$scope.lastDaysChoosen = 'wybierz ostatnie';
		}
			  
		//------------------------------------------------------------------------
		function ResetMonthsLabel( skipReset )
		{
			if ( ! skipReset )
				$scope.buttonDaysStyle 	= {}
			$scope.buttonMonthsStyle = {};
			$scope.buttonMonthsStyle['color'] = '#888888';

		  $scope.monthChoosen = 'wybierz miesi\u0105c';
		}
	
	  $scope.templates	= myTemplates;

	  $scope.cruId = $routeParams.cruId;
	  if (typeof($scope.cruId) === "undefined" )
		  $scope.cruId = 0;

	  $scope.registryName = Config[$scope.cruId][0];
	  
	  $scope.raportData = 0;
	  $scope.allTags = {};
	  $scope.statsTotal = {};
	  $scope.statsRecords = {};
	  $scope.statsDeailsShow = [];
	  $scope.statsDeailsButtonLabel = [];
	  $scope.total = { 'prize' : 0, 'count' : 0 };
	  
	  $scope.months = [];
	  $scope.months[0] = { id: 1, 'month': 'Czerwiec',	 	'monthId' : '06', 'year' : 2013 };
	  $scope.months[1] = { id: 2, 'month': 'Lipiec', 		'monthId' : '07', 'year' : 2013 };
	  $scope.months[2] = { id: 3, 'month': 'Sierpie\u0144',	'monthId' : '08', 'year' : 2013 };
	  $scope.months[3] = { id: 4, 'month': 'Wrzesie\u0144',	'monthId' : '09', 'year' : 2013 };
	  $scope.months[4] = { id: 5, 'month': 'Pazd\u017aiernik', 	'monthId' : '10', 'year' : 2013 };
	  $scope.months[5] = { id: 6, 'month': 'Listopad', 		'monthId' : '11', 'year' : 2013 };
	  $scope.months[6] = { id: 7, 'month': 'Grudzie\u0144',	'monthId' : '12', 'year' : 2013 };
	  $scope.months[7] = { id: 8, 'month': 'Stycze\u0144',	'monthId' : '01', 'year' : 2014 };
	  $scope.months[8] = { id: 9, 'month': 'Luty', 			'monthId' : '02', 'year' : 2014 };

	  $scope.lastDays 		= [];
	  $scope.lastDays[0] 	= {id: 7, name : '7 dni' };
	  $scope.lastDays[1] 	= {id: 14, name : '14 dni' }
	  $scope.lastDays[2] 	= {id: 30, name : '30 dni' };
	  $scope.lastDays[3] 	= {id: 60, name : '60 dni' }
	  $scope.lastDays[4] 	= {id: 90, name : '90 dni' }


	  ResetDaysLabel();
	  ResetMonthsLabel( true );
	
	  var dateStart = '0000-00-00';
	  var dateStop = '2050-01-30';
	  

	  var urlData;
	  urlData = PHPBackend+'/getRaportsData.php?cruId=' + $scope.cruId;
	  urlData += '&dateStart=' + dateStart;
	  urlData += '&dateStop=' + dateStop;
	  urlData += "&callback=JSON_CALLBACK";
	  
	  $http({method:'JSONP', url: urlData }).
	  	success(function(data)
	  	{
	  		$scope.raportData = data;
	  		
	  		var urlTags;
			urlTags = PHPBackend+'/getAllTags.php?cruId=' + $scope.cruId;
			urlTags += "&callback=JSON_CALLBACK";

	  		$http({method:'JSONP', url: urlTags }).
	  			success(function(data)
	  			{
	  				angular.forEach(data, function(value, key) {
	  					var tag;
//	  					tag = $scope.checkTag( data[key]['tag'] );
	  					tag = data[key]['tag'];
	  					$scope.allTags[tag] = data[key];
	  				});
	  				
	  				var tag = null;
	  				tag = $scope.checkTag( tag );
	  				$scope.allTags[tag] = {'id' : -1 , 'tag' : tag };
	  				
//	  		  		$scope.refresh();
	  		  		$scope.setLastDays( {id: 30, name: "30 dni" } );
	  			}).
	  			error(function(data,status,headers,config)
	  			{
	  				console.log("error " + status);
	  			});
	  	}).
		error(function(data, status, headers, config) {
		});

	  //---------------------------------------------------------------------------
	  $scope.toggleTagDetails = function( tag )
	  {
		  var cssId =  "#"+ $scope.getCssId( tag );
		  jQuery( cssId ).slideToggle();
		  
		  $scope.statsDeailsShow[tag] = !$scope.statsDeailsShow[tag];
		  if ( $scope.statsDeailsShow[tag] )
			  $scope.statsDeailsButtonLabel[tag] = "Schowaj";
		  else
			  $scope.statsDeailsButtonLabel[tag] = "Pokaz";
	  };
	  
	  $scope.setDateFromInput = function(startDate, stopDate)
	  {
		  ResetDaysLabel(); 
		  ResetMonthsLabel( true );
		  $scope.refresh( startDate, stopDate );
	  }
	  //---------------------------------------------------------------------------
	  $scope.checkTag = function(tag)
	  {
		  if ( typeof(tag) === 'undefined' || tag == null )
			  tag = '(nie przerobione)';
		  
		  return tag;
	  }
	  //---------------------------------------------------------------------------
	  $scope.getCssId = function(tag)
	  {
		  if ( typeof(tag) === 'undefined' || tag == null )
			  tag = 'NULL';
		  var ret;
		  ret = tag.replace(/[^A-za-z0-9_-]/g, '');
		  ret = "cat_records_" + ret;
		  return ret;
	  }
	  //---------------------------------------------------------------------------
	  function getLastDayDate( year, monthId )
	  {
		  var d = new Date(year, monthId, 0);
		  var day;
		  var ret;
		  
		  day = d.getUTCDate() + 1;
		  ret = year + "-"+monthId+"-"+day;
		  return ret;
	  }

	  //---------------------------------------------------------------------------
	  $scope.getMonthId = function( monthName )
	  {
		  var monthId = 0 ;
//		  monthId = $scope.months.indexOf(monthName);
//		  
//		  if ( monthId < 10 )
//			  monthId = "0" + parseInt(monthId);
		  
		  return monthId;
	  }
  
	  //---------------------------------------------------------------------------
	  $scope.setLastDays = function( days )
	  {
		  var dayCount  = days.id;
		  
		  var today = new Date();
		  var day = today.getDate();
		  var month = today.getMonth()+1;
		  var year = today.getFullYear();
		  
		  if ( day < 10 )
			  day = "0" + day;

		  if ( month < 10 )
			  month = "0" + month;
		  
		  $scope.stopDate = year + "-" + month + "-" +day;
		  
		  
		  var past = new Date();
		  past.setDate( today.getDate() - dayCount + 1 );
		  
		  month = past.getMonth()+1;
		  year = past.getFullYear();
		  day = past.getDate();

		  if ( day < 10 )
			  day = "0" + day;

		  if ( month < 10 )
			  month = "0" + month;
		  
		  $scope.startDate = year + "-" + month + "-" +day;

		  $scope.lastDaysChoosen = "ostatnie: " + days.name;
		  $scope.refresh( $scope.startDate, $scope.stopDate );
		  
		  ResetMonthsLabel();
	  }
	  
	  //---------------------------------------------------------------------------
	  $scope.setMonth = function( monthObj )
	  {
		  $scope.monthChoosen = monthObj.year + " " + monthObj.month;

		  $scope.startDate = monthObj.year + "-" + monthObj.monthId +"-01";
		  $scope.stopDate = getLastDayDate( monthObj.year, monthObj.monthId );
		  
//		  console.log($scope.strtDate + " date " + $scope.stopDate );
		  $scope.refresh( $scope.startDate, $scope.stopDate );
		  
		  ResetDaysLabel();
	  }
	  //---------------------------------------------------------------------------
	  $scope.setYear = function( year )
	  {
//		  $scope.yearChoosen = year;
//
//		  $scope.startDate = $scope.yearChoosen + "-" + $scope.getMonthId( $scope.monthChoosen ) +"-01";
//		  $scope.stopDate = getLastDayDate( $scope.yearChoosen, $scope.monthChoosen );
//		  
//		  $scope.refresh( $scope.startDate, $scope.stopDate );
	  }
	  //---------------------------------------------------------------------------

	  $scope.refresh = function( startDate, stopDate )
	  {
		  $scope.startDate = startDate;
		  $scope.stopDate = stopDate;
		  
//		  console.log("refresh " + $scope.startDate + " " + $scope.stopDate);
		  
		  $scope.resetRaport();
		  $scope.setRaport();
	  }
	  //---------------------------------------------------------------------------
	  $scope.resetRaport = function()
	  {
		  angular.forEach( $scope.allTags, function(value, key) 
			{
				var tagObj;
				var tag;
				var tagId;
				 
				tagObj = $scope.allTags[key];
				tag = $scope.checkTag( tagObj['tag'] );
				tagId = tagObj['id']
			    
	    		$scope.statsTotal[tag] = { 'tag' : tag, 'tagId' : tagId, 'cena': 0, 'count' : 0 };
	    		$scope.statsDeailsButtonLabel[tag] = "Pokaz";
	    		$scope.statsRecords[tag] = {};
	    		$scope.statsDeailsShow[tag] = false;
			});
	  }
	  

	  //---------------------------------------------------------------------------
	  $scope.setRaport = function()
	  {
		$scope.total = { 'prize' : 0, 'count' : 0 };
    	angular.forEach($scope.raportData, function(value, key) {
			
		  	var tag 	= $scope.checkTag( $scope.raportData[key]['tag'] );
			var tagId 	= $scope.raportData[key]['tag_id'];
			var cena 	= $scope.raportData[key]['cena'];
			var umowaId = $scope.raportData[key]['umowa_id'];
			var data	= $scope.raportData[key]['data'];

			if ( data < $scope.startDate || data > $scope.stopDate )
				return;
			$scope.total['prize'] += parseFloat(cena);
			$scope.total['count'] += 1;
			 	 
	    	$scope.statsTotal[tag]['cena'] += parseFloat(cena); 
		    $scope.statsTotal[tag]['count'] += 1;
			$scope.statsRecords[tag][umowaId] = $scope.raportData[key];
		});
    	//-----
    	// get rid of empty categories
  	  	angular.forEach( $scope.allTags, function(value, key) 
  			{
  				var tagObj;
  				var tag;
  				 
  				tagObj = $scope.allTags[key];
  				tag = $scope.checkTag( tagObj['tag'] );

  				if ( $scope.statsTotal[tag]['count'] == 0 )
  				{
  					delete $scope.statsTotal[tag];
  				}
  			});
	  }
  }]);


