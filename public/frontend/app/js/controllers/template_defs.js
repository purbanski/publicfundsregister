myTemplates 		= [];
myTemplates.search	= [];
myTemplates.common	= [];
myTemplates.stats	= [];
myTemplates.raports	= [];
//---------------------------------------------------------------------------
myTemplates.search.pagination 	= { name: 'search-pagination', url: 'partials/search/box/search/pagination.html'};
myTemplates.search.input		 = { name: 'search-input', url: 'partials/search/box/search/input.html'};

myTemplates.search.box = [];
myTemplates.search.box.main 	= { name: 'search-box-main', url: 'partials/search/box/main.html'};
myTemplates.search.box.output	= { name: 'search-box-output', url: 'partials/search/box/search/output.html'};
myTemplates.search.box.sorter	= { name: 'search-box-sorter', url: 'partials/search/box/search/sorter.html'};

myTemplates.search.table = [];
myTemplates.search.table.main	= { name: 'search-table-main', url: 'partials/search/table/main.html'};
//---------------------------------------------------------------------------	  
myTemplates.umowa = [];
myTemplates.umowa.box = [];

myTemplates.umowa.box.main		= { name: 'umowa-box-umowa', url: 'partials/search/box/umowa/umowa.html'};
myTemplates.umowa.box.comments	= { name: 'umowa-box-comments', url: 'partials/search/box/umowa/comments.html'};
myTemplates.umowa.box.files		= { name: 'umowa-box-files', url: 'partials/search/box/umowa/files.html'};
//---------------------------------------------------------------------------
myTemplates.common.top_menu	 	= { name: 'comp-top-menu', url: 'partials/common/top-menu.html'};

//---------------------------------------------------------------------------
myTemplates.stats.by_month = [];
myTemplates.stats.by_month.main	= { name: 'stats-by_month-main', url: 'partials/stats/by-months/by-months.html'};
myTemplates.stats.by_month.year	= { name: 'stats-by_month-year', url: 'partials/stats/by-months/year.html'};

//---------------------------------------------------------------------------
myTemplates.raports.cat_entries = { name: 'raports-cat-entries', url: 'partials/raports/raport-cat-entries.html'};
myTemplates.raports.header 		= { name: 'raports-header',		 url: 'partials/raports/raport-header.html'};

//---------------------------------------------------------------------------
