'use strict';

var FrontendUrl = 'http://trup.blackted.com';
var BackendUrl = 'http://trup.blackted.com';
var PHPBackend = BackendUrl+'/json-api2/api';

FrontendUrl = 'http://trupdev.blackted.com';
BackendUrl = 'http://trupdev.blackted.com';
PHPBackend = BackendUrl+'/json-api2/api';

var Config = new Array();

Config[0] = new Array();
Config[0][0] = 'Urz\u0105d Miasta Szczecin';

Config[1] = new Array();
Config[1][0] = 'Urz\u0105d Miasta Milan\u00f3w';

Config[2] = new Array();
Config[2][0] = 'Ministerstwo Kultury i Dziedzictwa Narodowego';

Config[3] = new Array();
Config[3][0] = 'Urz\u0105d Marsza\u0142kowski Wojew\u00f3dztwa Zachodniopomorskiego';

Config[4] = new Array();
Config[4][0] = 'Urz\u0105d Miasta O\u0142awa';

Config[5] = new Array();
Config[5][0] = 'Urz\u0105d Gminy K\u0119ty';

Config[69] = new Array();
Config[69][0] = 'Testowy';


//----------------------------------------------------------------------
function puGetData( dataStr )
{
	var d;
	d = dataStr.split('-');
	return new Date(d[0], d[1], d[2]);
}
//----------------------------------------------------------------------
function puGetDataSafe( dataStr, fallbackDataStr )
{
	var d;

	if ( typeof(dataStr) === "undefined" )
	{
		d = fallbackDataStr.split('-');
		return new Date(d[0], d[1], d[2]);
	}
	
	return puGetData( dataStr );
}
//----------------------------------------------------------------------
function puCapitaliseFirstLetter(string)
{
    return string.charAt(0).toUpperCase() + string.slice(1);
}
