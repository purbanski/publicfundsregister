<?php 
//------------------------------------------------------------------------------
require_once 'config.php';
//------------------------------------------------------------------------------
function pu_mysql_to_json( $sqlResult )
{
	$rows = array();
	while($r = mysql_fetch_assoc($sqlResult)) 
	{
    	$rows[] = $r;
	}
	return json_encode($rows);	
}
//------------------------------------------------------------------------------
function pu_mysql_connect()
{
	$con = mysql_connect($cfg['DBServer'], $cfg['DBUsername'], $cfg['DBPassword']);
	if (!$con) 
	{
		die('Could not connect: ' . mysql_error());
	}
	mysql_query("SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'", $con);
	
	return $con;
}
//------------------------------------------------------------------------------
function pu_check_for_hacking( $file )
{
	// not good .. / " ' ! ^ |
	$pattern = '/(^\.\.|\/|"|\'|\!|\^|\|)/';
	preg_match($pattern, $file, $matches, PREG_OFFSET_CAPTURE);
	if ( count($matches))
		die('hacking in progress...');
}
//------------------------------------------------------------------------------

?>