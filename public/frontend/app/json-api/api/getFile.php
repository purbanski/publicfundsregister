<?php 
//------------------------------------------------------------------------------
require_once '../config.php';
require_once '../toolbox.php';
//------------------------------------------------------------------------------

if ( !isset($_GET['f']) )
	die('missing argument');

$dir = '/var/www/html/cru/public/frontend/app/upload/';
$file = $_GET['f'];

// not good .. / " ' ! ^ |
pu_check_for_hacking( $file );

// check if it works in production
// $file = htmlspecialchars($file);

$file = $dir.$file;

if (file_exists($file)) {
	header('Content-Description: File Transfer');
	header('Content-Type: application/octet-stream');
	header('Content-Disposition: attachment; filename='.basename($file));
	header('Content-Transfer-Encoding: binary');
	header('Expires: 0');
	header('Cache-Control: must-revalidate');
	header('Pragma: public');
	header('Content-Length: ' . filesize($file));
	ob_clean();
	flush();
	readfile($file);
	exit;
}
else 
{
 	print "Nie znaleziono pliku";
}

//------------------------------------------------------------------------------
?>
