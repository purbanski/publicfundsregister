<?php 
//------------------------------------------------------------------------------
require_once '../config.php';
require_once '../toolbox.php';
//------------------------------------------------------------------------------
if ( !isset($_GET['uid']) )
	die('missing argument');

$umowaId = $_GET['uid'];

if (!is_numeric($umowaId))
	die();

$sql = "
		SELECT 
			id AS id,
			umowa_id AS umowaId,
			opis AS opis,
			plik AS plik 
		FROM CRU.backend_plik
		WHERE umowa_id=$umowaId;";

//---
$con = mysql_connect($cfg['DBServer'], $cfg['DBUsername'], $cfg['DBPassword']);
if (!$con)
{
	die('Could not connect: ' . mysql_error());
}
mysql_query("SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'", $con);
//---

$result = mysql_query($sql,$con);
$callback = $_GET['callback'];

echo $callback.'('.pu_mysql_to_json($result).');';
mysql_close($con);
//------------------------------------------------------------------------------
?>
