<?php 

//------------------------------------------------------------------------------
require_once '../config.php';
require_once '../toolbox.php';
//------------------------------------------------------------------------------
$sql = "
	SELECT 
		umowaTab.id 	AS umowaId,
		umowaTab.umowa 	AS umowa,
		umowaTab.data 	AS data,
		umowaTab.firma 	AS zlecenioBiorca,
		umowaTab.cena 	AS cena,
		umowaTab.opis 	AS umowaOpis,
		plikTab.count 	AS plikCount
	FROM CRU.backend_umowa umowaTab
	LEFT JOIN 
		( 
			SELECT
					umowa_id,
					count(*) as count
			FROM CRU.backend_plik 
			GROUP BY umowa_id
		) plikTab
	ON umowaTab.id = plikTab.umowa_id
	ORDER BY umowaTab.data DESC;";


//---
$con = mysql_connect($cfg['DBServer'], $cfg['DBUsername'], $cfg['DBPassword']);
if (!$con)
{
	die('Could not connect: ' . mysql_error());
}
mysql_query("SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'", $con);
//---

$result = mysql_query($sql,$con);
$callback = $_GET['callback'];

echo $callback.'('.pu_mysql_to_json($result).');';
mysql_close($con);
//------------------------------------------------------------------------------
?>
