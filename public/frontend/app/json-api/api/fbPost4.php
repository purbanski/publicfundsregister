<?php 
require '../config.php';

function puGetData()
{
	$data = strip_tags($_REQUEST['data']);
	$data = json_decode($data);
	
	$dataConv = array();
	foreach ( $data as $key => $value )
		$dataConv[$key] = $value;
	
	return $dataConv;	
}

function puGetUrl()
{
	$dataConv = puGetData();
	$ret = $cfg['FrontendUrl'];
	$ret = 'http://trup.blackted.com';
	$ret .= "/#/search/" . $dataConv['search'] ;
	$ret .= "/".$dataConv['dateFrom'];
	$ret .= "/".$dataConv['dateTo'];
	
	return $ret;
}

function puGetText()
{
	$data = puGetData();
	$search = $data['search'];
	$ret = "";
	
	if (strlen( $search ) > 1 )
	{
		$ret .= " Szukana fraza: " . $search. ".";
	}
	
	if (strlen( $data['dateFrom'] ) > 1 )
	{
		$ret .= " Data od: " . $data['dateFrom'] . ".";
	}

	if (strlen( $data['dateTo'] ) > 1 )
	{
		$ret .= " Data do: " . $data['dateTo'] . ".";
	}
	return $ret;
}


?>	
<!doctype html>
<html lang="en">

<head prefix="og: http://ogp.me/ns# product: http://ogp.me/ns/product#">
	<title>Totalny Rejestr Um&#243;w Publicznych - Szczecin</title>

	<meta property="fb:app_id" content="801715806509923" />
	<meta property="og:image" content="http://trup.blackted.com/img/logo2.png"/>
 	<meta property="og:type" content="website"/>

 	<meta property="og:title" content="TRUP - Szczecin"/>
    <meta property="og:site_name" content="Totalny Rejestr Um&#243;w Publicznych - Szczecin"/>
	<meta property="og:description" content="Totalny Rejestr Um&#243;w Publicznych - Szczecin. <?php echo puGetText(); ?>"/> 
		
	<meta http-equiv="refresh" content="1; url=<?php echo puGetUrl(); ?>" />
</head>

<body>
</body>
</html>